using UnityEngine;
using System.Collections;

public class AirAmmo : Ammo
{
    public override void cast()
    {
        transform.position = rootPosition;
    }

    public override void fly(){}
    public override void update()
    {
        if (1 == markForDestruction)
        {
            flying = false;
            hitting = true;
            StartCoroutine(DelayedDestroy(1f));
            StartCoroutine(DelayedDestroyObject(0.05f, mainObject));
            markForDestruction = 2;
        }
    }

    public override void init()
    {
        mainObject = Instantiate(preMain);
        mainObject.transform.parent = transform;
        mainObject.transform.localPosition = Vector3.zero;
        mainObject.transform.localRotation = Quaternion.Euler(0, -90, 0);
    }

    void initHit()
    {
        if (null == hitObject)
        {
            hitObject = Instantiate(preHit);
            hitObject.transform.parent = transform;
            hitObject.transform.localPosition = Vector3.zero;
            hitObject.transform.localRotation = Quaternion.Euler(0, -90, 0);
            hitObject.transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (0 == markForDestruction && flying && flyTime > 0.3f)
        {
            switch (col.gameObject.tag)
            {
                case "FireAmmo":
                    if (col.gameObject.GetComponent<FireAmmo>().flying)
                    {
                        markForDestruction = 1;
                    }
                    break;
                case "AirAmmo":
                    if (col.gameObject.GetComponent<AirAmmo>().flying)
                    {
                        markForDestruction = 1;
                    }
                    break;
                case "WaterAmmo":
                    if (col.gameObject.GetComponent<WaterAmmo>().flying)
                    {
                        markForDestruction = 1;
                    }
                    break;
                case "Avatar":
                    Linker.manager.avatar.hit();
                    markForDestruction = 1;
                    break;
                case "NPC":
                    col.gameObject.GetComponent<Npc>().hit();
                    markForDestruction = 1;
                    break;
                case "Wall":
                    markForDestruction = 1;
                    break;
            }
        }
    }

    public override void hitAnimation()
    {
        hittingTime += Time.deltaTime;
        if (hittingTime > 0.15f)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            initHit();
        }
    }
}