using UnityEngine;
using System.Collections;

public abstract class Ammo : MonoBehaviour
{

    public bool flying;
    public bool hitting;
    public float flyTime;
    public float hittingTime;
    public int markForDestruction;
    public Vector3 target;
    public GameObject targetObject;

    public GameObject preMain;
    public GameObject preTail;
    public GameObject preHit;
    public GameObject secondLevelHit;
    public bool isSecondStatePassed;
    
    private float flySpeedMultiplier;

    [HideInInspector] public GameObject mainObject;
    [HideInInspector] public GameObject hitObject;

    public Vector3 rootPosition;
    public Vector3 leftHandPosition;
    public Vector3 leftArmDirection;
    public Vector3 rightHandPosition;
    public Vector3 rightArmDirection;

    private int counter;

    private void Start()
    {
        isSecondStatePassed = false;
        markForDestruction = 0;
        counter = 0;
        init();
        hitting = false;
        hittingTime = 0;
        flyTime = 0;
        transform.parent = Linker.manager.world.transform;

        flySpeedMultiplier = 100 * (1 + (0.3f * Linker.manager.level));
//        targetObject = GameObject.Find("ST");
//        target = targetObject.GetComponent<Transform>().position;
    }

    private void Update()
    {
        if (flying)
        {
            flyTime += Time.deltaTime;
            fly();
        }
        else if (hitting)
        {
            hitAnimation();
        }
        else
        {
            cast();
        }

        update();
    }

    public void activateSecondState()
    {
        isSecondStatePassed = true;
        GameObject secondStateBlow = Instantiate(secondLevelHit);
        secondStateBlow.transform.parent = transform;
        secondStateBlow.transform.localPosition = Vector3.zero;
    }

    public void startFly()
    {
        if (!flying)
        {
            GetComponent<Rigidbody>().constraints = 0;
            transform.LookAt(target);
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
            GetComponent<Rigidbody>().AddForce((target - transform.position).normalized * flySpeedMultiplier, ForceMode.Acceleration);
            flying = true;
        }
    }
    
    public abstract void cast();
    public abstract void fly();
    public abstract void update();
    public abstract void init();
    public abstract void hitAnimation();

    public IEnumerator DelayedDestroy(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(transform.gameObject);
    }

    public IEnumerator DelayedDestroyObject(float seconds, GameObject target)
    {
        yield return new WaitForSeconds(seconds);
        Destroy(target.transform.gameObject);
    }

    public void setLeftArm(Vector3 position, Vector3 direction)
    {
        leftHandPosition = position;
        leftArmDirection = direction;
    }
    
    public void setRightArm(Vector3 position, Vector3 direction)
    {
        rightHandPosition = position;
        rightArmDirection = direction;
    }
    
    public void setRootPosition(Vector3 position)
    {
        rootPosition = position;
    }

    public void cancel()
    {
        markForDestruction = 1;
    }

    public void setTarget(Vector3 position)
    {
        target = position;
    }
}