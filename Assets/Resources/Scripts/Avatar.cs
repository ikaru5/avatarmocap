using System;
using System.Collections;
using UnityEngine;
using Ventuz.OSC;
using System.Security.AccessControl;

public class Avatar : MonoBehaviour
{
    enum State
    {
        AirStart,
        AirCast,
        FireStart,
        FireCast,
        WaterStart,
        WaterCast,
        EarthStart,
        EarthCast
    }

    public GameObject preAir = null;
    public GameObject preFire = null;
    public GameObject preWater = null;
    public GameObject preEarth = null;

    public GameObject currentAmmonition = null;
    
    public int currentState = -1;
    public int lastMatchedState = -1;
    public int stateRepeats = 0;
    public int maxStateRepeats = 10;
    public float stateAge = 0;
    
    public static Matrix4x4[] matrix = new Matrix4x4[21];
    private Character character;
    public Transform[] bones;
    public ComparablePosition currentPosition;
    public Recorder recorder;
    public PositionParser parser;
    public ParserLogic parserLogic;
    public GameObject collider;
    public int position = 0;
    private float hitProgress = 0;
    
    public Avatar(String characterType)
    {
        preAir = GameObject.Find("Avatar").GetComponent<AvatarInterface>().preAir;
        preWater = GameObject.Find("Avatar").GetComponent<AvatarInterface>().preWater;
        preEarth = GameObject.Find("Avatar").GetComponent<AvatarInterface>().preEarth;
        preFire = GameObject.Find("Avatar").GetComponent<AvatarInterface>().preFire;
        
        reset();
        
        bones = new Transform[21];
        
        switch (characterType)
        {
            default:
                character = new SphereCharacter(GameObject.FindGameObjectWithTag("AvatarRoot"));
                break;
        }

        recorder = new Recorder(bones);
        recorder.load("hit", 1, new Vector3(0.0f,0,0.65f));
        parserLogic = new ParserLogic();
        parser = new PositionParser(parserLogic.positionFilenames, ref parserLogic);
        
        currentPosition = new ComparablePosition();
        collider = GameObject.Find("AvatarCollider");
        Linker.manager.avatar = this;
    }

    public void reset()
    {
        currentState = -1;
        stateRepeats = 0;
        stateAge = 0;
        lastMatchedState = -1;
    }

    public void hit()
    {
        if (-3 != currentState)
        {
            Debug.Log("hit avatar");
            if (null != currentAmmonition)
            {
                currentAmmonition.GetComponent<Ammo>().cancel();
                currentAmmonition = null;
            }
            currentState = -3;
            lastMatchedState = -1;
            recorder.setCurrentPlayRecord(1);
            recorder.interpolate = true;
            recorder.loop = false;
            recorder.setPlayBackSpeed(1.8f);
            recorder.rewind();
            recorder.play();
            hitProgress = 0;
            position++;
        }
    }

    public void moveDownToNextPosition(float progress)
    {
        Vector3 oldPosition = Linker.manager.positions[0][1][position].transform.position;
        Vector3 newPosition = Linker.manager.positions[0][1][position].transform.position;
        
    }

    public void update()
    {
        //calculateAimTarget();
        //Linker.manager.debugDirection.transform.LookAt(
        //    Linker.manager.debugDirection.transform.position + (currentPosition.getGazeVector() * 1000)
        //    );
        collider.transform.position = character.getRootPosition();
        recorder.update();

        stateAge += Time.deltaTime;
        
//        currentAmmonition.GetComponent<AirAmmo>().setLeftArm(currentPosition.getLeftHandPosition(), currentPosition.getLeftArmDirection());
//        currentAmmonition.GetComponent<AirAmmo>().setRightArm(currentPosition.getRightHandPosition(), currentPosition.getRightArmDirection());
        switch (currentState)
        {
            case (int) State.AirCast:
                parseCast("air");
                if (lastMatchedState == 1 && !currentAmmonition.GetComponent<AirAmmo>().isSecondStatePassed)
                {
                    currentAmmonition.GetComponent<AirAmmo>().activateSecondState();
                }
                if (currentAmmonition == null) currentAmmonition = Instantiate(preAir);
                currentAmmonition.GetComponent<AirAmmo>().setRootPosition(character.getRootPosition());
                if (lastMatchedState == -2)
                {
                    currentAmmonition.GetComponent<AirAmmo>().cancel(); 
                    currentAmmonition = null;
                }
                if (lastMatchedState == -1)
                {
                    currentAmmonition.GetComponent<AirAmmo>().setTarget(calculateAimTarget((int)State.AirCast));
                    currentAmmonition.GetComponent<AirAmmo>().startFly();
                    currentAmmonition = null;
                }
                break;
            case (int) State.FireCast:
                parseCast("fire");
                if (lastMatchedState == 1 && !currentAmmonition.GetComponent<FireAmmo>().isSecondStatePassed)
                {
                    currentAmmonition.GetComponent<FireAmmo>().activateSecondState();
                }
                if (currentAmmonition == null) currentAmmonition = Instantiate(preFire);
                currentAmmonition.GetComponent<FireAmmo>().setLeftArm(character.getLeftHandPosition(), character.getLeftArmDirection());
                if (lastMatchedState == -2)
                {
                    currentAmmonition.GetComponent<FireAmmo>().cancel(); 
                    currentAmmonition = null;
                }
                if (lastMatchedState == -1)
                {
                    currentAmmonition.GetComponent<FireAmmo>().setTarget(calculateAimTarget((int)State.FireCast));
                    currentAmmonition.GetComponent<FireAmmo>().startFly();
                    currentAmmonition = null;
                }
                break;
            case (int) State.EarthCast:
                parseCast("earth");
                if (currentAmmonition == null)
                {
                    currentAmmonition = Instantiate(preEarth);
                    currentAmmonition.GetComponent<EarthAmmo>().setEarthSource(0, 1, position);
                }

                if (lastMatchedState == 1 && currentAmmonition.GetComponent<EarthAmmo>().state != 1)
                {
                    currentAmmonition.GetComponent<EarthAmmo>().setRootPosition(character.getRootPosition());
                    currentAmmonition.GetComponent<EarthAmmo>().setState(1);
                }
                
                if (lastMatchedState == -2)
                {
                    currentAmmonition.GetComponent<EarthAmmo>().cancel(); 
                    currentAmmonition = null;
                }
                if (lastMatchedState == -1)
                {
                    currentAmmonition.GetComponent<EarthAmmo>().setTarget(calculateAimTarget((int)State.EarthCast));
                    currentAmmonition.GetComponent<EarthAmmo>().startFly();
                    currentAmmonition = null;
                }
                break;
            case (int) State.WaterCast:
                parseCast("water");
                if (lastMatchedState == 1 && !currentAmmonition.GetComponent<WaterAmmo>().isSecondStatePassed)
                {
                    currentAmmonition.GetComponent<WaterAmmo>().activateSecondState();
                }
                if (currentAmmonition == null) currentAmmonition = Instantiate(preWater);
                currentAmmonition.GetComponent<WaterAmmo>().setLeftArm(character.getLeftHandPosition(), character.getLeftArmDirection());
                if (lastMatchedState == -2)
                {
                    currentAmmonition.GetComponent<WaterAmmo>().cancel(); 
                    currentAmmonition = null;
                }
                if (lastMatchedState == -1)
                {
                    currentAmmonition.GetComponent<WaterAmmo>().setTarget(calculateAimTarget((int)State.WaterCast));
                    currentAmmonition.GetComponent<WaterAmmo>().startFly();
                    currentAmmonition = null;
                }
                break;
            case -3:
                // got hit
                if (-3 != lastMatchedState) Linker.manager.moveWorldTo(position - 1, position, hitProgress);
                if (recorder.playing)
                {
                    recorder.loadNextFrame();
                    hitProgress = Math.Min(hitProgress + Time.deltaTime, 1);
                }
                else
                {
                    recorder.setPlayBackSpeed(1);
                    if (-3 != lastMatchedState) Linker.manager.moveWorldTo(position - 1, position, 1);
                    lastMatchedState = -3;
                    if (position > 2)
                    {
                        Linker.manager.fallDown();
                        Debug.Log("GAME END");
                    }
                    else
                    {
                        currentState = -1;
                    }
                }
                break;
            default:
                parseStartPosition("air_start", (int) State.AirStart, (int) State.AirCast);
                parseStartPosition("fire_start", (int) State.FireStart, (int) State.FireCast);
                parseStartPosition("earth_start", (int) State.EarthStart, (int) State.EarthCast);
                parseStartPosition("water_start", (int) State.WaterStart, (int) State.WaterCast);
                break;
        }
        parser.compare("air_start", ref currentPosition);
    }

    Vector3 calculateAimTarget(int state = -1)
    {
        float angle = 0;
        if (state == (int)State.AirCast || state == (int)State.EarthCast || -1 == state)
        {
            Vector3 leftArmDirection = character.getLeftArmDirection().normalized;
            Vector2 leftArmDirection2D = new Vector2(leftArmDirection.x, leftArmDirection.z);
            float angleLeft = Vector2.SignedAngle(leftArmDirection2D, Vector2.left);

            Vector3 rightArmDirection = character.getRightArmDirection().normalized;
            Vector2 rightArmDirection2D = new Vector2(rightArmDirection.x, rightArmDirection.z);
            float angleRight = Vector2.SignedAngle(rightArmDirection2D, Vector2.left);
            Debug.Log("angleLeft: " + angleLeft + " leftArmDirection2D: " + leftArmDirection2D);
            Debug.Log("angleRight: " + angleRight + " rightArmDirection2D: " + rightArmDirection2D);
            if (state == -1) return Vector3.zero; // TODO why do we do this?
            angle = (angleLeft + angleRight) / 2;
        }
        else
        {
            Vector3 leftArmDirection = character.getLeftArmDirection().normalized;
            Vector2 leftArmDirection2D = new Vector2(leftArmDirection.x, leftArmDirection.z);
            angle = Vector2.SignedAngle(leftArmDirection2D, Vector2.left);
        }

        // TODO np3 for npc 1
        // left
        if (angle < -20.0f && angle > -45.0f && null != Linker.manager.npc3 && Linker.manager.npc3.GetComponent<Npc>().isAlive)
        {
            Debug.Log("Shot at left");
            return Linker.manager.npc3.GetComponent<Npc>().getAimPosition();
        }

        // mid
        if (angle >= -20.0f && angle <= 20.0f && null != Linker.manager.npc1 && Linker.manager.npc2.GetComponent<Npc>().isAlive)
        {
            Debug.Log("Shot at mid");
            return Linker.manager.npc2.GetComponent<Npc>().getAimPosition();
        }

        // right
        if (angle > 20.0f && angle < 45.0f && null != Linker.manager.npc1 && Linker.manager.npc1.GetComponent<Npc>().isAlive)
        {
            Debug.Log("Shot at right");
            return Linker.manager.npc1.GetComponent<Npc>().getAimPosition();
        }

        float x = Mathf.Cos((Mathf.PI / 180) * (180 - angle));
        float y = Mathf.Sin((Mathf.PI / 180) * (180 - angle));
        Debug.Log("newAngle: " + (180 - angle) + " x: " + x + " y: " + y);
        Vector3 direction = new Vector3(x, 0, y);
        return character.getRootPosition() + (direction * 10);
    }

    void parseCast(string name)
    {
        if (0 == lastMatchedState && parser.compare(name + "_1", ref currentPosition))
        {
            Debug.Log(name + " Cast");
            lastMatchedState = 1;
            stateAge = 0;
        }
                
        if (1 == lastMatchedState && parser.compare(name + "_2", ref currentPosition))
        {
            Debug.Log(name + " Final");
            currentState = -1;
            lastMatchedState = -1;
            stateAge = 0;
        }

        if (stateAge > 10)
        {
            Debug.Log(name + " Aborted");
            currentState = -1;
            lastMatchedState = -2;
            stateAge = 0;
        }
    }
    void parseStartPosition(string name, int state, int targetState)
    {
        if (parser.compare(name, ref currentPosition))
        {
            if (state == lastMatchedState)
            {
                stateRepeats++;
                if (stateRepeats > maxStateRepeats)
                {
                    Debug.Log("Starting: " + name);
                    lastMatchedState = 0;
                    stateAge = 0;
                    currentState = targetState;
                }
            }
            else
            {
                lastMatchedState = state;
                stateRepeats = 0;
            }
        }
        else
        {
            if (state == lastMatchedState)
            {
                lastMatchedState = -1;
            }
        }
    }

    public void updateBones(float[] skeleton)
    {
        if (recorder.playing || recorder.showPosition)
        {
            recorder.loadNextFrame();

            for (int i = 0; i < 21; i++)
            {
                currentPosition.position.recordedPositions[i] = recorder.currentPlayPositions[i];
                currentPosition.position.recordedRotations[i] = recorder.currentPlayRotations[i];
                bones[i].transform.localPosition = recorder.currentPlayPositions[i];
                bones[i].transform.localRotation = recorder.currentPlayRotations[i];
                character.updateBone(i, bones[i].transform.localPosition, bones[i].transform.localRotation);
            }

        }
        else
        {
            //Speichern in Matrizen zur Weiterverarbeitung
            int x = 0;
            for (int i = 0; i <21; i++) {
                matrix[i].m00 = skeleton[x++];
                matrix[i].m01 = skeleton[x++];
                matrix[i].m02 = skeleton[x++];
                matrix[i].m03 = skeleton[x++];
                matrix[i].m10 = skeleton[x++];
                matrix[i].m11 = skeleton[x++];
                matrix[i].m12 = skeleton[x++];
                matrix[i].m13 = skeleton[x++];
                matrix[i].m20 = skeleton[x++];
                matrix[i].m21 = skeleton[x++];
                matrix[i].m22 = skeleton[x++];
                matrix[i].m23 = skeleton[x++];
                matrix[i].m30 = skeleton[x++];
                matrix[i].m31 = skeleton[x++];
                matrix[i].m32 = skeleton[x++];
                matrix[i].m33 = skeleton[x++];
            
                bones[i].transform.localRotation = MoCapUtils.GetRotation(matrix[i]);
                bones[i].transform.localPosition = MoCapUtils.GetPosition(matrix[i]);
                currentPosition.position.recordedPositions[i] = bones[i].transform.localPosition;
                currentPosition.position.recordedRotations[i] = bones[i].transform.localRotation;
                character.updateBone(i, bones[i].transform.localPosition, bones[i].transform.localRotation);
            } 
        }
        
    }
    
    public Vector3 getAimPosition()
    {
        return character.getRootPosition();
    }

}