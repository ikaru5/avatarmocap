using UnityEngine;

public abstract class Character
{
    abstract public void updateBone(int index, Vector3 position, Quaternion rotation);
    abstract public Vector3 getRootPosition();
    abstract public Vector3 getLocalRootPosition();
    abstract public Vector3 getLeftArmDirection();
    abstract public Vector3 getRightArmDirection();
    abstract public Vector3 getLeftHandPosition();
    abstract public Vector3 getRightHandPosition();
    abstract public void setCollidingLimbsActive(bool state);

}