using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ComparablePosition
{
    enum Bone
    {
        Root,            // 0   Upper Chest 	 Root
        MidTorso,        // 1   Mid Torso        Child of Upper Chest
        LowerTorso,      // 2   Lower Torso      Child of Mid Torso
        LeftUpperLeg,    // 3   Left Upper Leg	 Child of Lower Torso
        LeftLowerLeg,    // 4   Left Lower Leg	 Child of Left Upper Leg
        LeftFoot,        // 5   Left Foot		 Child of Left Lower Leg
        RightUpperLeg,   // 6   Right Upper Leg	 Child of Lower Torso
        RightLowerLeg,   // 7   Right Lower Leg	 Child of Right Upper Leg
        RightFoot,       // 8   Right Foot 		 Child of Right Lower Leg
        Neck,            // 9   Neck			 Child of Upper Chest
        Head,            // 10  Head 			 Child of Neck
        RightClavicle,   // 11  Right Clavicle	 Child of Upper Chest
        RightShoulder,   // 12  Right Shoulder	 Child of Clavicle
        RightUpperArm,   // 13  Right Upper Arm	 Child of Shoulder
        RightLowerArm,   // 14  Right Lower Arm	 Child of Upper Arm
        RightHand,       // 15  Right Hand		 Child of Lower Arm
        LeftClavicle,    // 16  Left Clavicle	 Child of Upper Chest
        LeftShoulder,    // 17  Left Shoulder	 Child of Left Clavicle
        LeftUpperArm,    // 18  Left Upper Arm	 Child of Left Shoulder
        LeftLowerArm,    // 19  Left Lower Arm	 Child of Left Upper Arm
        LeftHand         // 20  Left Hand		 Child of Left Lower Arm
    }

    
    public RecordedPosition position;
    
    public ComparablePosition(string filename)
    {
        position = RecordedPosition.load(filename);
    }
    
    public ComparablePosition()
    {
        position = new RecordedPosition();
    }

    public Vector3 getVectorBetween(int indexStart, int indexEnd)
    {
        return position.recordedPositions[indexEnd] - position.recordedPositions[indexStart];
    }

    // can also try to compare by vector3.distance
    public bool compare(int indexStart, int indexEnd, ref ComparablePosition comparable, int percentage = 80)
    {
        Vector3 me = getVectorBetween(indexStart, indexEnd).normalized;
        Vector3 other = comparable.getVectorBetween(indexStart, indexEnd).normalized;
        float distance = Vector3.Distance(me, other);
        float allowedFraction = percentage / 100.0f;
        
        Debug.Log("me_rot_start: " + position.recordedRotations[indexStart].eulerAngles);
        Debug.Log("other_rot_start: " + comparable.position.recordedRotations[indexStart].eulerAngles);

        Debug.Log("me_pos_start: " + position.recordedPositions[indexStart]);
        Debug.Log("me_pos_end: " + position.recordedPositions[indexEnd]);

        Debug.Log("other_pos_start: " + comparable.position.recordedPositions[indexStart]);
        Debug.Log("other_pos_end: " + comparable.position.recordedPositions[indexEnd]);
            
        Debug.Log("me: " + getVectorBetween(indexStart, indexEnd));
        Debug.Log("other: " + comparable.getVectorBetween(indexStart, indexEnd));

        Debug.Log("me_norm: " + me);
        Debug.Log("other_norm: " + other);
        Debug.Log("distance: " + distance + " - - 2.0 * allowedFraction - 2.0: " + ( 2.0 - 2.0 * allowedFraction));
        
        return 2.0 - 2.0 * allowedFraction > distance;
    }

    public bool compareByRotation(int indexStart, int indexEnd, ref ComparablePosition comparable, int percentage = 80)
    {
        Vector3 me = position.recordedRotations[indexStart].eulerAngles;
        Vector3 other = comparable.position.recordedRotations[indexStart].eulerAngles;
        me.y = 0;
        other.y = 0;
//        me = me.normalized;
//        other = other.normalized;
        float distance = Vector3.Distance(me, other);
        float allowedFraction = percentage / 100.0f;
        
        Debug.Log("indexStart: " + indexStart);
        Debug.Log("me: " + me);
        Debug.Log("other: " + other);
        Debug.Log("distance: " + distance + " - - 2.0 * allowedFraction - 2.0: " + ( 2.0 - 2.0 * allowedFraction));
        
        return 30 > distance;
    }

    public Vector3 getShoulderVector()
    {
        Vector3 rightShoulder = position.recordedPositions[17];
        Vector3 leftShoulder = position.recordedPositions[12];
        rightShoulder.y = 0;
        leftShoulder.y = 0;
        return (rightShoulder - leftShoulder);
    }

    public Vector3 getGazeVector() {
        return Quaternion.Euler(0, -90, 0) * getShoulderVector().normalized;
    }

    // nice and fast, but maybe also the distance to a additional root point? -> what about size
    // or maybe a second root vector? -> performance bottle neck?
    public bool compareByAngles(int indexStart, int indexEnd, ref ComparablePosition comparable, int percentage = 80)
    {
        // compare by distance
        if (percentage < 50)
        {
            float distMe = Vector3.Distance(position.recordedPositions[indexStart], position.recordedPositions[indexEnd]);
            float distOther = Vector3.Distance(comparable.position.recordedPositions[indexStart], comparable.position.recordedPositions[indexEnd]);

            //if (percentage == 40) Debug.Log();
            bool returnVal = ((float)percentage / 100.0f) > Math.Abs(distMe - distOther);
            if (percentage == 40) Debug.Log("distMe: " + distMe + " distOther: " + distOther + " out: " + returnVal + " Math.Abs(distMe - distOther): " + Math.Abs(distMe - distOther));
            return returnVal;
        }
        // Working       
        Vector3 me = getVectorBetween(indexStart, indexEnd).normalized;
        Vector3 other = comparable.getVectorBetween(indexStart, indexEnd).normalized;

        float myAngle1 = Vector3.Angle(getGazeVector(), me);
        float otherAngle1 = Vector3.Angle(comparable.getGazeVector(), other);

        float myAngle2 = Vector3.Angle(Vector3.up, me);
        float otherAngle2 = Vector3.Angle(Vector3.up, other);

        float myAngle3 = Vector3.Angle(getShoulderVector(), me);
        float otherAngle3 = Vector3.Angle(comparable.getShoulderVector(), other);

        // Working End 

        //Vector3 me = getVectorBetween(indexStart, indexEnd).normalized;
        //Vector3 other = comparable.getVectorBetween(indexStart, indexEnd).normalized;

        //float gazeAngle = Vector2.SignedAngle(new Vector2(me.x, me.z), new Vector2(getGazeVector().x, getGazeVector().z));
        //float otherGazeAngle = Vector2.SignedAngle(new Vector2(other.x, other.z), new Vector2(comparable.getGazeVector().x, comparable.getGazeVector().z));

        //Vector3 myRootSpineVector = (position.recordedPositions[10] - position.recordedPositions[9]).normalized;
        //Vector3 otherRootSpineVector = (comparable.position.recordedPositions[10] - comparable.position.recordedPositions[9]).normalized;
 

        //Debug.Log("myAngle1: " + myAngle1 + " - - otherAngle: " + otherAngle1 + " - - indexStart: " + indexStart + " - - indexEnd: " + indexEnd);
        //Debug.Log("myAngle2: " + myAngle2 + " - - otherAngle: " + otherAngle2 + " - - indexStart: " + indexStart + " - - indexEnd: " + indexEnd);
        
//        Debug.Log(Vector3.SignedAngle(me, myRootVector, myRootVector));
//        Debug.Log(Vector3.SignedAngle(other, otherRootVector, otherRootVector));

        int maxFail = 50;
        switch (percentage)
        {
            case 50:
                maxFail = 50;
                break;
            case 60:
                maxFail = 40;
                break;
            case 70:
                maxFail = 30;
                break;
            case 80:
                maxFail = 20;
                break;
            case 90:
                maxFail = 10;
                break;
        }
        //Debug.Log("out: " + (maxFail > Math.Abs(myAngle1 - otherAngle1) && maxFail > Math.Abs(myAngle2 - otherAngle2)));
        //Debug.Log("gazeAngle: " + gazeAngle + " otherGazeAngle: " + otherGazeAngle);
        //return maxFail > Math.Abs(myAngle1 - otherAngle1) && maxFail > Math.Abs(myAngle2 - otherAngle2) && maxFail > Math.Abs(gazeAngle - otherGazeAngle);
        return maxFail > Math.Abs(myAngle1 - otherAngle1) && maxFail > Math.Abs(myAngle2 - otherAngle2) && maxFail > Math.Abs(myAngle3 - otherAngle3);
    }

    

    public bool batchCompare(int[][] compareInstructions, ref ComparablePosition position)
    {
        foreach (int[] instructionSet in compareInstructions)
        {
            if (instructionSet.Length == 3)
            {
                if (!compareByAngles(instructionSet[0], instructionSet[1], ref position, instructionSet[2])) 
                    return false;
            }
            else
            {
                if (!compareByAngles(instructionSet[0], instructionSet[1], ref position)) return false;
            }
        }
        
        return true;
    }
}