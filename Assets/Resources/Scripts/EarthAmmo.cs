using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class EarthAmmo : Ammo
{
    public Vector3 startPosition;
    public int state;
    
    private float moveProgress;
    private Vector3 targetPosition;
    

    public void setEarthSource(int team, int side, int position)
    {
        GameObject source = Linker.manager.positions[team][side][position].GetComponent<ArenaPosition>().earthSource;
        source.GetComponent<EarthSource>().reloadAmmo();
        startPosition = source.transform.position;
        transform.position = startPosition;
        targetPosition = startPosition + new Vector3(0, 1.4f, 0);
        state = 0;
    }

    public void setState(int state)
    {
        moveProgress = 0;
        this.state = state;
    }
    
    public override void cast()
    {
        if (0 == state)
        {
            move(1.4f, moveProgress);
            
        } else if (state == 1)
        {
            if (moveProgress == 0) startPosition = transform.position;
            targetPosition = rootPosition + new Vector3(0, 1, 0);
            move(1.4f, moveProgress);
        }
    }

    void move(float duration, float progress)
    {
        moveProgress += Time.deltaTime;
        if (progress >= 1.4f)
        {
            transform.position = targetPosition;
        }
        else
        {
            transform.position = easeOutQuad(startPosition, targetPosition, progress, duration);
        }
    }

    private Vector3 easeOutQuad(Vector3 a, Vector3 b, float timePassed, float duration)
    {
        return a + (b - a) * easeOutQuad(timePassed, 0.0f, 1.0f, duration);
    }
    public static float easeOutQuad(float t, float b, float c, float d)
    {
        t /= d;
        return -c * t*(t-2) + b;
    }

    public override void init()
    {
        // Nothing 
    }

    public override void fly(){}
    public override void update()
    {
        if (1 == markForDestruction)
        {
            flying = false;
            hitting = true;
            StartCoroutine(DelayedDestroy(1f));
            StartCoroutine(DelayedDestroyObject(0.1f, transform.Find("PartA").gameObject));
            StartCoroutine(DelayedDestroyObject(0.1f, transform.Find("PartB").gameObject));
            initHit();
            markForDestruction = 2;
        }
    }
    

    void initHit()
    {
        hitObject = Instantiate(preHit);
        hitObject.transform.parent = transform;
        hitObject.transform.localPosition = Vector3.zero;
        hitObject.transform.localRotation = Quaternion.Euler(0, -90, 0);
        hitObject.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
    }

    private void OnTriggerEnter(Collider col)
    {
        if (0 == markForDestruction && flying && flyTime > 0.3f)
        {
            switch (col.gameObject.tag)
            {
                case "FireAmmo":
                    if (col.gameObject.GetComponent<FireAmmo>().flying)
                    {
                        markForDestruction = 1;
                    }
                    break;
                case "AirAmmo":
                    if (col.gameObject.GetComponent<AirAmmo>().flying)
                    {
                        markForDestruction = 1;
                    }
                    break;
                case "EarthAmmo":
                    if (col.gameObject.GetComponent<EarthAmmo>().flying)
                    {
                        markForDestruction = 1;
                    }
                    break;
                case "Avatar":
                    Linker.manager.avatar.hit();
                    markForDestruction = 1;
                    break;
                case "NPC":
                    col.gameObject.GetComponent<Npc>().hit();
                    markForDestruction = 1;
                    break;
                case "Wall":
                    markForDestruction = 1;
                    break;
            }
        }
    }
    public override void hitAnimation()
    {
        hittingTime += Time.deltaTime;
        if (hittingTime > 0.3f) GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}