using System;
using UnityEngine;

public class EarthSource : MonoBehaviour
{
    public Vector3 originPosition;
    private bool moving;
    private float moveProgress;
    private Vector3 displacement;
    
    private void Start()
    {
        originPosition = transform.localPosition;
        moving = false;
        displacement = new Vector3(0,- 0.1f, 0);
    }

    private void Update()
    {
        if (moving)
        {
            moveProgress += Time.deltaTime * 0.6f;
            if (moveProgress >= 1)
            {
                transform.localPosition = originPosition;
                moveProgress = 0;
                moving = false;
            }
            else
            {
                transform.localPosition = Vector3.Lerp(originPosition + displacement, originPosition, moveProgress);
            }
        } 
    }

    public void reloadAmmo()
    {
        moving = true;
    }
}