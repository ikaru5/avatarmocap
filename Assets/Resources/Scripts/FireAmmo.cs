using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class FireAmmo : Ammo
{

    public override void cast()
    {
        transform.position = leftHandPosition + leftArmDirection.normalized * 0.1f;
    }

    public override void fly(){}
    public override void update()
    {
        if (1 == markForDestruction) {
            flying = false;
            hitting = true;
            StartCoroutine(DelayedDestroy(1f));
            StartCoroutine(DelayedDestroyObject(0.2f, mainObject));
            initHit();
            markForDestruction = 2;
        }
    }

    public override void init(){
        mainObject = Instantiate(preMain);
        mainObject.transform.parent = transform;
        mainObject.transform.localPosition = Vector3.zero;
        mainObject.transform.localRotation = Quaternion.Euler(0,-90,0);
    }

    void initHit()
    {
        hitObject = Instantiate(preHit);
        hitObject.transform.parent = transform;
        hitObject.transform.localPosition = Vector3.zero;
        hitObject.transform.localRotation = Quaternion.Euler(0, -90, 0);
    }

    private void OnTriggerEnter(Collider col)
    {
        if (0 == markForDestruction && flying && flyTime > 0.3f)
        {
            switch (col.gameObject.tag)
            {
                case "FireAmmo":
                    if (col.gameObject.GetComponent<FireAmmo>().flying)
                    {
                        markForDestruction = 1;
                    }
                    break;
                case "WaterAmmo":
                    if (col.gameObject.GetComponent<WaterAmmo>().flying)
                    {
                        markForDestruction = 1;
                    }
                    break;
                case "EarthAmmo":
                    if (col.gameObject.GetComponent<EarthAmmo>().flying)
                    {
                        markForDestruction = 1;
                    }
                    break;
                case "Avatar":
                    Linker.manager.avatar.hit();
                    markForDestruction = 1;
                    break;
                case "NPC":
                    col.gameObject.GetComponent<Npc>().hit();
                    markForDestruction = 1;
                    break;
                case "Wall":
                    markForDestruction = 1;
                    break;
            }
        }
    }
    public override void hitAnimation()
    {
        hittingTime += Time.deltaTime;
        if (hittingTime > 0.3f) GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}