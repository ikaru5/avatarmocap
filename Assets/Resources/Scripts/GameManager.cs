﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public GameObject debugDirection;
	
	public GameObject posBlueLeft1;
	public GameObject posBlueLeft2;
	public GameObject posBlueLeft3;
	public GameObject posBlueLeft4;
	public GameObject posBlueMid1;
	public GameObject posBlueMid2;
	public GameObject posBlueMid3;
	public GameObject posBlueMid4;
	public GameObject posBlueRight1;
	public GameObject posBlueRight2;
	public GameObject posBlueRight3;
	public GameObject posBlueRight4;
	
	public GameObject posRedLeft1;
	public GameObject posRedLeft2;
	public GameObject posRedLeft3;
	public GameObject posRedLeft4;
	public GameObject posRedMid1;
	public GameObject posRedMid2;
	public GameObject posRedMid3;
	public GameObject posRedMid4;
	public GameObject posRedRight1;
	public GameObject posRedRight2;
	public GameObject posRedRight3;
	public GameObject posRedRight4;

	public GameObject enemiesTxt;
	public GameObject teammatesTxt;
	public GameObject headlineTxt;
	public GameObject levelTxt;

	public GameObject mocCapCenter;
	public GameObject world;

	[HideInInspector] public GameObject[][][] positions; // [0: Blue; 1 Red] - [0: Left; 1: Mid; 2: Right] - [...]

	public GameObject preNpc;
	// enemy NPCs
	[HideInInspector] public GameObject npc1;
	[HideInInspector] public GameObject npc2;
	[HideInInspector] public GameObject npc3;
	
	// allied NPCs
	[HideInInspector] public GameObject npc4;
	[HideInInspector] public GameObject npc5;

	[HideInInspector] public Avatar avatar;
	
	public int level;
	private float levelTxtTime;
	private float gameOverTime;
	private float levelClearedTime;
	public bool isGameOver = false;
	
    StartButton startButton;

    private Vector3 worldOriginPosition;
    private Vector3 worldPositionBackup;
    private Vector3 moveVector;

    void Awake()
    {
	    Linker.manager = this;
        startButton = FindObjectOfType<StartButton>();
    }
	// Use this for initialization
	void Start ()
	{
		buildPositionArray();
		worldOriginPosition = world.transform.position;
		levelTxtTime = 0;
		levelClearedTime = 0;
	}

	private void resetGame()
	{
		destroyNPCs();
		gameOverTime = 3.0f;
		isGameOver = false;
		headlineTxt.GetComponent<Text>().text = "PRESS GREEN BUTTON";
		startButton.gameObject.SetActive(true);
		world.transform.position = worldOriginPosition;
	}

	private int updateTxtCounters()
	{
		int enemiesAlive = 0;
		int teammatesAlive = 0;

		if (npc1.GetComponent<Npc>().position < 3) enemiesAlive++;
		if (npc2.GetComponent<Npc>().position < 3) enemiesAlive++;
		if (npc3.GetComponent<Npc>().position < 3) enemiesAlive++;
		if (npc4.GetComponent<Npc>().position < 3) teammatesAlive++;
		if (npc5.GetComponent<Npc>().position < 3) teammatesAlive++;

		teammatesTxt.GetComponent<Text>().text = "Teammates Alive: " + teammatesAlive;
		enemiesTxt.GetComponent<Text>().text = "Enemies Alive: " + enemiesAlive;
		levelTxt.GetComponent<Text>().text = "Current Level: " + level;
		return enemiesAlive;
	}
	
	private void destroyNPCs()
	{
		if (null != npc1) Destroy(npc1);
		if (null != npc2) Destroy(npc2);
		if (null != npc3) Destroy(npc3);
		if (null != npc4) Destroy(npc4);
		if (null != npc5) Destroy(npc5);
	}
	
	private void createNPCs()
	{
		destroyNPCs();
		
		npc1 = Instantiate(preNpc);
		npc1.transform.parent = world.transform;
		npc1.GetComponent<Npc>().setTeam(1,0,0, true);
		
		npc2 = Instantiate(preNpc);
		npc2.transform.parent = world.transform;
		npc2.GetComponent<Npc>().setTeam(1,1,0, true);
		
		npc3 = Instantiate(preNpc);
		npc3.transform.parent = world.transform;
		npc3.GetComponent<Npc>().setTeam(1,2,0, true);
		
		npc4 = Instantiate(preNpc);
		npc4.transform.parent = world.transform;
		npc4.GetComponent<Npc>().setTeam(0,0,0, true);
		
		npc5 = Instantiate(preNpc);
		npc5.transform.parent = world.transform;
		npc5.GetComponent<Npc>().setTeam(0,2,0, true);
	}

	public void newGame()
	{
		loadLevel(1);
		gameOverTime = 3.0f;
		isGameOver = false;
		startButton.gameObject.SetActive(false);
	}

	public void loadLevel(int level)
	{
		this.level = level;
		levelTxtTime = 3;
		world.transform.position = worldOriginPosition;
		createNPCs();
		avatar.position = 0;
		updateTxtCounters();
	}

	public void npcDown()
	{
		if (0 == updateTxtCounters())
		{
			levelClearedTime = 3;
			headlineTxt.GetComponent<Text>().text = "LEVEL " + level + " CLEARED";
		}
	}

	public void gameOver()
	{
		if (isGameOver)
		{
			headlineTxt.GetComponent<Text>().text = "GAME OVER";
			gameOverTime -= Time.deltaTime;
			if (gameOverTime < 0)
			{
				avatar.reset();
				resetGame();
			}
		}
	}

	public Vector3 getRandomEnemyPosition(int team)
	{
		Vector3[] enemyPositions = new Vector3[3];
		int enemyCount = 0;
		if (0 == team)
		{
			if (npc1.GetComponent<Npc>().position < 3)
			{
				enemyCount++;
				enemyPositions[enemyCount - 1] = npc1.GetComponent<Npc>().getAimPosition();
			}
			if (npc2.GetComponent<Npc>().position < 3)
			{
				enemyCount++;
				enemyPositions[enemyCount - 1] = npc2.GetComponent<Npc>().getAimPosition();
			}
			if (npc3.GetComponent<Npc>().position < 3)
			{
				enemyCount++;
				enemyPositions[enemyCount - 1] = npc3.GetComponent<Npc>().getAimPosition();
			}
		}
		else
		{
			if (npc4.GetComponent<Npc>().position < 3)
			{
				enemyCount++;
				enemyPositions[enemyCount - 1] = npc4.GetComponent<Npc>().getAimPosition();
			}
			if (npc5.GetComponent<Npc>().position < 3)
			{
				enemyCount++;
				enemyPositions[enemyCount - 1] = npc5.GetComponent<Npc>().getAimPosition();
			}
			if (avatar.position < 3)
			{
				enemyCount++;
				enemyPositions[enemyCount - 1] = avatar.getAimPosition();
			}
		}
		int randomNumber = Random.Range(0, enemyCount);
		return enemyPositions[randomNumber];

	}

	void buildPositionArray()
	{
		positions = new GameObject[2][][];
		positions[0] = new GameObject[3][];
		positions[1] = new GameObject[3][];

		positions[0][0] = new[] {posBlueLeft1, posBlueLeft2, posBlueLeft3, posBlueLeft4};
		positions[0][1] = new[] {posBlueMid1, posBlueMid2, posBlueMid3, posBlueMid4};
		positions[0][2] = new[] {posBlueRight1, posBlueRight2, posBlueRight3, posBlueRight4};
		
		positions[1][0] = new[] {posRedLeft1, posRedLeft2, posRedLeft3, posRedLeft4};
		positions[1][1] = new[] {posRedMid1, posRedMid2, posRedMid3, posRedMid4};
		positions[1][2] = new[] {posRedRight1, posRedRight2, posRedRight3, posRedRight4};
	}

	public void moveWorldTo(int originPos, int targetPos, float progress)
	{
		if (0 == progress)
		{
			worldPositionBackup = world.transform.position;
			moveVector = positions[0][1][targetPos].transform.position - positions[0][1][originPos].transform.position;
		}
		world.transform.position = Vector3.Lerp(worldPositionBackup, worldPositionBackup - moveVector, progress);
	}
	
	public void fallDown()
	{
		world.transform.position = 
			new Vector3(
				world.transform.position.x, 
				world.transform.position.y + (7 * Time.deltaTime), 
				world.transform.position.z
			);
		isGameOver = true;
	}
	
	
	// Update is called once per frame
    void Update()
    {
	    if (levelClearedTime > 0)
	    {
		    levelClearedTime -= Time.deltaTime;
		    if (levelClearedTime <= 0)
		    {
			    loadLevel(level + 1);
		    }
		    return;
	    }
	    
	    gameOver();

	    if (levelTxtTime > 0)
	    {
		    levelTxtTime -= Time.deltaTime;
		    if (levelTxtTime > 0)
		    {
			    headlineTxt.GetComponent<Text>().text = "LEVEL " + level;    
		    }
		    else
		    {
			    headlineTxt.GetComponent<Text>().text = "";
		    }
		    
	    }
    }


}
