using System;
using System.Collections;
using UnityEngine;
using Ventuz.OSC;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class MoCapAnsteuerung : MonoBehaviour
{
	private	static UdpReader reader;
	private OscBundle bundle;
	//Handmesh bei Fernglas an und Ausschalten
	public SkinnedMeshRenderer[] hands;
    public GameManager manager;
	private bool mocap, mocapcontrolling;
	private float[] skeleton = new float[336]; // Matrix of 4x4 x count=21;
	
	// interpolation
	private float[] skeletonBackup = new float[336];
	private float[] skeletonTarget = new float[336];
	
	private float interpolationProgress = 0f;
	public float handabstand, handlinks, handrechts = 0;
	public Transform _00UpperChest;
	public Transform _01MidTorso;
	public Transform _02LowerTorso;
	public Transform _03LeftUpperLeg;
	public Transform _04LeftLowerLeg;
	public Transform _05LeftFoot;
	public Transform _06RightUpperLeg;
	public Transform _07RightLowerLeg;
	public Transform _08RightFoot;
	public Transform _09Neck;
	public Transform _10Head;
	public Transform _11RightClavicle;
	public Transform _12RightShoulder;
	public Transform _13RightUpperArm;
	public Transform _14RightLowerArm;
	public Transform _15RightHand;
	public Transform _16LeftClavicle;
	public Transform _17LeftShoulder;
	public Transform _18LeftUpperArm;
	public Transform _19LeftLowerArm;
	public Transform _20LeftHand;
	public float maxFernglas = 0.3f;
    public float maxclap = 0.3f;
	public static bool glas;
	private Vector3 initPosition = Vector3.zero;
	private Transform AvatarRoot;
    private int posenumber = 0, previouspose = 0;
    private Avatar avatar;

	void Start ()
	{
		if(reader == null)
		reader = new UdpReader (4000);
		AvatarRoot = GameObject.FindGameObjectWithTag("AvatarRoot").transform;
		
		avatar = new Avatar("default");
		avatar.bones [0] = _00UpperChest;
		avatar.bones [1] = _01MidTorso;
		avatar.bones [2] = _02LowerTorso;
		avatar.bones [3] = _03LeftUpperLeg;
		avatar.bones [4] = _04LeftLowerLeg;
		avatar.bones [5] = _05LeftFoot;
		avatar.bones [6] = _06RightUpperLeg;
		avatar.bones [7] = _07RightLowerLeg;
		avatar.bones [8] = _08RightFoot;
		avatar.bones [9] = _09Neck;
		avatar.bones [10] = _10Head;
		avatar.bones [11] = _11RightClavicle;
		avatar.bones [12] = _12RightShoulder;
		avatar.bones [13] = _13RightUpperArm;
		avatar.bones [14] = _14RightLowerArm;
		avatar.bones [15] = _15RightHand;
		avatar.bones [16] = _16LeftClavicle;
		avatar.bones [17] = _17LeftShoulder;
		avatar.bones [18] = _18LeftUpperArm;
		avatar.bones [19] = _19LeftLowerArm;
		avatar.bones [20] = _20LeftHand;
	}

	private void FixedUpdate()
	{
		if (posenumber != 0 && interpolationProgress <= 1.0f)
		{
			int length = skeleton.Length;
			for (int i = 0; i < length; i++) {
				skeleton[i] = Mathf.Lerp(skeletonBackup[i], skeletonTarget[i], interpolationProgress);
			}

			interpolationProgress += Time.deltaTime;
		}
	}

	private void backupSkeleton()
	{
		int length = skeleton.Length;
		for (int i = 0; i < length; i++) {
			skeletonBackup[i] = skeleton[i];
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
        
		/*	BoneInfos
		Index	Bone Name		Position in Hierarchy
		0		Upper Chest 	Root
		1		Mid Torso		Child of Upper Chest
		2		Lower Torso		Child of Mid Torso
		3		Left Upper Leg	Child of Lower Torso
		4		Left Lower Leg	Child of Left Upper Leg
		5		Left Foot		Child of Left Lower Leg
		6		Right Upper Leg	Child of Lower Torso
		7		Right Lower Leg	Child of Right Upper Leg
		8		Right Foot 		Child of Right Lower Leg
		9		Neck			Child of Upper Chest
		10		Head 			Child of Neck
		11		Right Clavicle	Child of Upper Chest
		12		Right Shoulder	Child of Clavicle
		13		Right Upper Arm	Child of Shoulder
		14		Right Lower Arm	Child of Upper Arm
		15		Right Hand		Child of Lower Arm
		16		Left Clavicle	Child of Upper Chest
		17		Left Shoulder	Child of Left Clavicle
		18		Left Upper Arm	Child of Left Shoulder
		19		Left Lower Arm	Child of Left Upper Arm
		20		Left Hand		Child of Left Lower Arm
		*/
		
		// Avatar instance needs to be updated manually
		avatar.update();
		
		#region MoCap Ansteuerung
		if (reader != null)
			bundle = (OscBundle)reader.Receive (); //Aktuelle MoCap Position empfangen
        
        //bundle = null; //for testing not connected environment
        
        if (bundle != null)
        {
            int i = 0;
            foreach (OscElement m in bundle.Elements)
            {
                skeleton[i] = (float)m.Args[0];
                i++;
            }
            //print("Recieved Bundle");

            //Save for once on pressed button
            if (Input.GetKeyDown(KeyCode.F1))
            {
                print("Saving F1");
                saveSkeleton("pose1");
            }

			#region Saving Multiple Positions

			if (Input.GetKeyDown(KeyCode.F2))
            {
                saveSkeleton("pose2");
            }
			if (Input.GetKeyDown(KeyCode.F3))
			{
				saveSkeleton("pose3");
			}
			if (Input.GetKeyDown(KeyCode.F4))
			{
				saveSkeleton("pose4");
			}
			if (Input.GetKeyDown(KeyCode.F5))
			{
				saveSkeleton("pose5");
			}
			if (Input.GetKeyDown(KeyCode.F6))
			{
				saveSkeleton("pose6");
			}
			if (Input.GetKeyDown(KeyCode.F7))
			{
				saveSkeleton("pose7");
			}
			if (Input.GetKeyDown(KeyCode.F8))
			{
				saveSkeleton("pose8");
            }
            if (Input.GetKeyDown(KeyCode.F9))
            {
                saveSkeleton("pose9");
            }
            if (Input.GetKeyDown(KeyCode.F10))
            {
                saveSkeleton("pose10");
            }							
			
			#endregion

            mocap = true;
            mocapcontrolling = false;
        }
        else
        {
            //Load for once if no bundle found
            //if successfull, set mocap true, else quit

            if(posenumber == 0) //no pose has been set yet
            {
                mocap = loadSkeleton(skeleton, "pose6");
                mocap = loadSkeleton(skeletonTarget, "pose6");
                posenumber = 6;
            }
            
            #region LoadPoses

            if (Input.GetKeyDown(KeyCode.Alpha1) && posenumber != 1)
            {
	            backupSkeleton();
	            interpolationProgress = 0;
                mocap = loadSkeleton(skeletonTarget, "pose1");
                posenumber = 1;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) && posenumber != 2)
            {
	            backupSkeleton();
	            interpolationProgress = 0;
                mocap = loadSkeleton(skeletonTarget, "pose2");
                posenumber = 2;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3) && posenumber != 3)
            {
	            backupSkeleton();
	            interpolationProgress = 0;
                mocap = loadSkeleton(skeletonTarget, "pose3");
                posenumber = 3;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4) && posenumber != 4)
            {
	            backupSkeleton();
	            interpolationProgress = 0;
                mocap = loadSkeleton(skeletonTarget, "pose4");
                posenumber = 4;
            } 
            if (Input.GetKeyDown(KeyCode.Alpha5) && posenumber != 5)
            {
	            backupSkeleton();
	            interpolationProgress = 0;
                mocap = loadSkeleton(skeletonTarget, "pose5");
                posenumber = 5;
            }
            if (Input.GetKeyDown(KeyCode.Alpha6) && posenumber != 6)
            {
                mocap = loadSkeleton(skeleton, "pose6");
                posenumber = 6;
            } 
            if (Input.GetKeyDown(KeyCode.Alpha7) && posenumber != 7)
            {
                mocap = loadSkeleton(skeleton, "pose7");
                posenumber = 7;
            }
            if (Input.GetKeyDown(KeyCode.Alpha8) && posenumber != 8)
            {
                mocap = loadSkeleton(skeleton, "pose8");
                posenumber = 8;
            }
            if (Input.GetKeyDown(KeyCode.Alpha9) && posenumber != 9)
            {
                mocap = loadSkeleton(skeleton, "pose9");
                posenumber = 9;
            }
            if (Input.GetKeyDown(KeyCode.Alpha0) && posenumber != 10)
            {
                mocap = loadSkeleton(skeleton, "pose10");
                posenumber = 10;
            }
            #endregion

            mocapcontrolling = true;

            if (Input.GetKeyDown(KeyCode.C))
            {
                if (posenumber != 7)
                {
                    previouspose = posenumber;
                    loadSkeleton(skeleton, "pose7");
                    posenumber = 7;
                }
            }
            if (Input.GetKeyUp(KeyCode.C))
            {
                mocap = loadSkeleton(skeleton, "pose" + previouspose);
                posenumber = previouspose;    
            }
        }
		

        if (mocap) {
            avatar.updateBones(skeleton);

            //Manual Controlling
            if (mocapcontrolling)
            {                
                if (Input.GetKey(KeyCode.C))
                {
                    AvatarRoot.transform.Translate(new Vector3(Input.GetAxis("Vertical") * -0.03f, 0, Input.GetAxis("Horizontal") * 0.03f));
                }
                else
                {   
                    AvatarRoot.transform.Translate(new Vector3(Input.GetAxis("Vertical") * -0.06f, 0, Input.GetAxis("Horizontal") * 0.06f));
                }

                if (Input.GetKey(KeyCode.Q))
                {
                    AvatarRoot.Rotate(new Vector3(0, 1, 0), -5f);
                }
                if (Input.GetKey(KeyCode.E))
                {
                    AvatarRoot.Rotate(new Vector3(0, 1, 0), 5f);
                }
                
                #region LimittoPlayarea
                float avatarx, avatarz;
                if (AvatarRoot.position.z > 1.7f)
                {
                    avatarz = 1.7f;
                }else{    
                    if (AvatarRoot.position.z < -1.5f)
                    {
                        avatarz = -1.5f;
                    }else{
                        avatarz = AvatarRoot.position.z;
                    }
                }
                if (AvatarRoot.position.x > 1.3f)
                {
                    avatarx = 1.3f;
                }
                else
                {
                    if (AvatarRoot.position.x < -1.3f)
                    {
                        avatarx = -1.3f;
                    }
                    else
                    {
                        avatarx = AvatarRoot.position.x;
                    }
                }

                AvatarRoot.transform.position = new Vector3(avatarx, 0, avatarz);
                #endregion 
            }
            else //controlling via MoCap, locking manual controlling
            {
                AvatarRoot.transform.position = new Vector3(0,0,0);
                AvatarRoot.rotation = Quaternion.Euler(0f, 0f, 0f);

//                if (initPosition == Vector3.zero) initPosition = sphere[1].transform.localPosition;
//                //Rotation im Stillstand
//                if (sphere[1].transform.localPosition.x > initPosition.x - 0.01f && sphere[1].transform.localPosition.x < initPosition.x + 0.01f &&
//                    sphere[1].transform.localPosition.y > initPosition.y - 0.01f && sphere[1].transform.localPosition.y < initPosition.y + 0.01f &&
//                    sphere[1].transform.localPosition.z > initPosition.z - 0.01f && sphere[1].transform.localPosition.z < initPosition.z + 0.01f)
//                {
//                    //AvatarRoot.rotation = Quaternion.Euler(0f,90f,0f);
//                    //Debug.Log("Still");
//                }
//                else
//                {
//                    AvatarRoot.rotation = Quaternion.Euler(0f, 0f, 0f);
//                    //Debug.Log ("Bewegt");
//                }
            }

            /*
			//Head and Neck
			bones [9].Rotate (180, 180, 0);
			//bones[10].Rotate(180,180,0);
			//left Arm
			for (int i=16; i<=20; i++)
				bones [i].Rotate (0, 90, 90);
			//right Arm
			for (int i=11; i<=15; i++)
				bones [i].Rotate (0, -90, -90);
			//Foots
			bones [5].Rotate (90, 0, 0);
			bones [8].Rotate (90, 0, 0);



	

			//Fernglastest
			//9= Nacken 10 = Kopf 15=Handrechts 20=Handlinks
			if (Math.Abs ((MoCapUtils.GetPosition (matrix [10]) - MoCapUtils.GetPosition (matrix [15])).magnitude) < maxFernglas && 
                Math.Abs ((MoCapUtils.GetPosition (matrix [10]) - MoCapUtils.GetPosition (matrix [20])).magnitude) < maxFernglas && 
                Math.Abs ((MoCapUtils.GetPosition (matrix [15]) - MoCapUtils.GetPosition (matrix [20])).magnitude) < maxFernglas) {
				glas = true; //Fernglas Aktiv
				for (int i = 0; i < hands.Length; i++)
					hands [i].enabled = false;
			} else {
				glas = false; //Fernglas nicht aktiv
				for (int i = 0; i < hands.Length; i++)
					hands [i].enabled = true;
				
			}
            */


        }
        #endregion
    }

	//Allowing to Save/Load Avatars Base Position from MoCap to OfflineVersion
    public void saveSkeleton(string filename){
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.dataPath + "/" + filename + ".dat");

        AvatarData data = new AvatarData();
        data.skeleton = new float[336];

        int length = this.skeleton.Length;
        for (int i = 0; i < length; i++)
        {
            data.skeleton[i] = this.skeleton[i];
        }

        bf.Serialize(file, data);
        file.Close();
        print("File " + filename + " has been saved");
    }

    public bool loadSkeleton(float[] skeletonarray, String filename) {
        if (File.Exists(Application.dataPath + "/" + filename + ".dat")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.dataPath + "/" + filename + ".dat", FileMode.Open);
            AvatarData data = (AvatarData)bf.Deserialize(file);
            file.Close();

            int length = data.skeleton.Length;
            for (int i = 0; i < length; i++) {
                skeletonarray[i] = data.skeleton[i];
            }

            return true;
        }
        return false;
    }
}

//Allowing to Save/Load Avatars Base Position from MoCap to OfflineVersion

[Serializable]
class AvatarData { 
    public float[] skeleton;
}