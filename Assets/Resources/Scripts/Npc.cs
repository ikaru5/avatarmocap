using UnityEngine;
using System.Collections;

public class Npc : MonoBehaviour
{
    enum State
    {
        AirCast,
        FireCast,
        WaterCast,
        EarthCast,
        Idle,
        FinishedAction,
        Hit,
        Out
    }

    enum RecordNumber
    {
        Air,
        Fire,
        Water,
        Earth,
        Idle,
        Hit
    }

    enum Bender
    {
        Air,
        Fire,
        Earth,
        Water
    }

    public GameObject preAir = null;
    public GameObject preFire = null;
    public GameObject preWater = null;
    public GameObject preEarth = null;

    public GameObject currentAmmonition = null;
    public ComparablePosition currentPosition;
    public int currentState = -1;
    public float idleTime;
    public float idleTimePassed;
    
    private Character character;
    public Recorder recorder;
    public string characterType;
    
    private GameObject originPositionRef;
    private GameObject targetPositionRef;
    private bool move = false;
    private float moveProgress;
    public int benderType;

    public int team;
    public int side;
    public int position;

    private float lookAtTargetProgress = 0;
    private Quaternion lookAtTarget;
    private Quaternion lookAtOrigin;
    private bool turning = false;
    private bool turningBack = false;
    
    private Vector3 aimTarget;

    public bool isAlive = true;
    
    // TODO remove
    public int lastOut = -1;
    
    void Start()
    {
        idleTime = 0;
        idleTimePassed = 0;
        moveProgress = 0;
        
        currentState = (int) State.FinishedAction;
        currentPosition = new ComparablePosition();

        switch (characterType)
        {
            default:
                character = new SphereCharacter(transform.gameObject);
                break;
        }

        recorder = new Recorder(new Transform[21], true, 6, 0);
        recorder.load("air", (int) RecordNumber.Air, new Vector3(-0.3f,0,0));
        recorder.load("fire", (int) RecordNumber.Fire, new Vector3(-0.6f,0,0.3f));
        recorder.load("earth", (int) RecordNumber.Earth, new Vector3(-1.2f,0,0.2f));
        recorder.load("water", (int) RecordNumber.Water, new Vector3(-0.9f,0,0f));
        recorder.load("idle", (int) RecordNumber.Idle, new Vector3(-0.6f,0,0.85f));
        recorder.load("hit", (int) RecordNumber.Hit, new Vector3(-1.0f,0,0.75f));
        recorder.interpolate = true;
        
        setBenderType();

        character.setCollidingLimbsActive(false);
    }

    public void hit()
    {
        if (-3 != currentState)
        {
            if (position < 3)
            {
                if (null != currentAmmonition)
                {
                    currentAmmonition.GetComponent<Ammo>().cancel();
                    currentAmmonition = null;
                }
                recorder.setCurrentPlayRecord((int) RecordNumber.Hit);
                recorder.interpolate = false;
                recorder.loop = false;
                recorder.rewind(0.2f);
                recorder.play();
                setTeam(team, side, position + 1);
                currentState = (int) State.Hit;    
            }
        }
    }

    public void setBenderType(int typeNumber = -1)
    {
        if (-1 != typeNumber)
        {
            benderType = typeNumber;
        }
        else
        {
            benderType = Random.Range(0, 4);
        }
    }

    public void setTeam(int team, int side, int position, bool force = false)
    {
        this.team = team;
        this.side = side;
        this.position = position;
        
        if (force)
        {
            int oppositeTeam = team == 0 ? 1 : 0;
            int oppositeSide = side == 1 ? 1 : (side == 0 ? 2 : 0);
            transform.position = Linker.manager.positions[team][side][position].transform.position;
            lookAt(Linker.manager.positions[oppositeTeam][oppositeSide][position].transform.position);
        }
        else
        {
            moveProgress = 0;
            move = true;
            originPositionRef = Linker.manager.positions[team][side][position - 1];
            targetPositionRef = Linker.manager.positions[team][side][position];    
            
        }
        lookAtOrigin = transform.rotation;
    }

    public void lookAt(Vector3 target)
    {
        transform.LookAt(target);
        transform.Rotate(new Vector3(0,90,0));
    }

    public void updateMove()
    {
        if (move)
        {
            moveProgress += Time.deltaTime;
            if (moveProgress >= 1)
            {
                transform.position = Vector3.Lerp(originPositionRef.transform.position, targetPositionRef.transform.position, 1);
                moveProgress = 0;
                move = false;
            }
            else
            {
                transform.position = Vector3.Lerp(originPositionRef.transform.position, targetPositionRef.transform.position, moveProgress);
            }
        }
    }

    void turnTo(Vector3 target)
    {
        lookAtTargetProgress = 0;
        lookAt(target);
        lookAtTarget = transform.rotation;
        transform.rotation = lookAtOrigin;
        turning = true;
    }

    void updateTurn()
    {
        if (turning)
        {
            lookAtTargetProgress += Time.deltaTime * 0.6f;
            if (lookAtTargetProgress >= 1)
            {
                transform.rotation = lookAtTarget;
                lookAtTargetProgress = 0;
                turning = false;
            }
            else
            {
                transform.rotation = Quaternion.Lerp(lookAtOrigin, lookAtTarget, lookAtTargetProgress);
            }
        }

        if (turningBack)
        {
            lookAtTargetProgress += Time.deltaTime * 0.6f;
            if (lookAtTargetProgress >= 1)
            {
                transform.rotation = lookAtOrigin;
                lookAtTargetProgress = 0;
                turningBack = false;
            }
            else
            {
                transform.rotation = Quaternion.Lerp(lookAtTarget, lookAtOrigin, lookAtTargetProgress);
            }
        }
    }
    
    void Update()
    {
        gameObject.GetComponent<BoxCollider>().center = character.getLocalRootPosition() + new Vector3(0,-0.5f,0);
        updateMove();
        updateTurn();
        recorder.update();

        switch (currentState)
        {
            case (int) State.AirCast:
                airShot();
                break;
            case (int) State.EarthCast:
                earthShot();
                break;
            case (int) State.WaterCast:
                waterShot();
                break;
            case (int) State.FireCast:
                fireShot();
                break;
            case (int) State.Idle:
                idleTimePassed += Time.deltaTime;
                if (idleTimePassed > idleTime)
                {
                    idleTimePassed = 0;
                    idleTime = 0;
                    initAttack();
                }
                break;
            case (int) State.FinishedAction:
                turningBack = true;
                if (Linker.manager.level == 1) idleTime = Random.Range(0.5f, 5.0f);
                if (Linker.manager.level == 2) idleTime = Random.Range(0.3f, 4.0f);
                if (Linker.manager.level == 3) idleTime = Random.Range(0.1f, 3.0f);
                if (Linker.manager.level == 4) idleTime = Random.Range(0.1f, 2.0f);
                if (Linker.manager.level >= 5) idleTime = Random.Range(0.1f, 1.0f);
                idleTimePassed = 0;
                currentState = (int) State.Idle;
                recorder.setCurrentPlayRecord((int) RecordNumber.Idle);
                recorder.setPlayBackSpeed(1.0f);
                recorder.loop = true;
                recorder.rewind();
                recorder.play();
                break;
            case (int) State.Hit:
                if (!recorder.playing)
                {
                    currentState = (int) State.FinishedAction;
                    if (position > 2)
                    {
                        currentState = (int) State.Out;
                        isAlive = false;
                        Linker.manager.npcDown();
                    }
                }
                break;
            case (int) State.Out: 
                transform.position = 
                    new Vector3( 
                        transform.position.x,  
                        transform.position.y - (7 * Time.deltaTime),
                        transform.position.z
                    );
                break;
        }
        
        if (recorder.playing)
        {
            recorder.loadNextFrame();
        }

        for (int i = 0; i < 21; i++)
        {
            currentPosition.position.recordedPositions[i] = recorder.currentPlayPositions[i];
            currentPosition.position.recordedRotations[i] = recorder.currentPlayRotations[i];
            character.updateBone(i, currentPosition.position.recordedPositions[i], currentPosition.position.recordedRotations[i]);
        }
    }

    void initAttack()
    {
        float speedGroth = 0.3f;
        switch (benderType)
        {
           case (int) Bender.Air:
               currentState = (int) State.AirCast;
               recorder.setCurrentPlayRecord((int) RecordNumber.Air);
               recorder.setPlayBackSpeed(1.3f + (speedGroth * (Linker.manager.level - 1)));
               break;
           case (int) Bender.Earth:
               currentState = (int) State.EarthCast;
               recorder.setCurrentPlayRecord((int) RecordNumber.Earth);
               recorder.setPlayBackSpeed(1.5f + (speedGroth * (Linker.manager.level - 1)));
               break;
           case (int) Bender.Water:
               currentState = (int) State.WaterCast;
               recorder.setCurrentPlayRecord((int) RecordNumber.Water);
               recorder.setPlayBackSpeed(1.2f + (speedGroth * (Linker.manager.level - 1)));
               break;
           case (int) Bender.Fire:
               currentState = (int) State.FireCast;
               recorder.setCurrentPlayRecord((int) RecordNumber.Fire);
               recorder.setPlayBackSpeed(1.5f + (speedGroth * (Linker.manager.level - 1)));
               break;
        }

        recorder.loop = false;
        recorder.rewind();
        recorder.play();
    }

    void earthShot()
    {
        if (!recorder.interpolating && recorder.lastPlayPosition > 8 && currentAmmonition == null)
        {
            currentAmmonition = Instantiate(preEarth);
            aimTarget = Linker.manager.getRandomEnemyPosition(team);
            currentAmmonition.GetComponent<EarthAmmo>().setTarget(aimTarget);
            currentAmmonition.GetComponent<EarthAmmo>().setEarthSource(team, side, position);
        }

        if (recorder.lastPlayPosition > 15 && currentAmmonition.GetComponent<EarthAmmo>().state != 1)
        {
            currentAmmonition.GetComponent<EarthAmmo>().setRootPosition(character.getRootPosition());
            currentAmmonition.GetComponent<EarthAmmo>().setState(1);
        }

        if (!recorder.interpolating && !turning && recorder.lastPlayPosition > 10 && recorder.lastPlayPosition < 13)
        {
            turnTo(aimTarget);
        }
        
        if (!recorder.playing)
        {
            currentState = (int) State.FinishedAction;
            currentAmmonition.GetComponent<EarthAmmo>().startFly();
            currentAmmonition = null;
        }
    }
    
    void airShot()
    {
        if (!recorder.interpolating && recorder.lastPlayPosition > 5 && currentAmmonition == null)
        {
            currentAmmonition = Instantiate(preAir);
            aimTarget = Linker.manager.getRandomEnemyPosition(team);
            currentAmmonition.GetComponent<AirAmmo>().setTarget(aimTarget);
        }
        if (currentAmmonition != null) currentAmmonition.GetComponent<AirAmmo>().setRootPosition(character.getRootPosition());

        if (!recorder.interpolating && !turning && recorder.lastPlayPosition > 10 && recorder.lastPlayPosition < 13)
        {
            turnTo(aimTarget);
        }
        
        if (!recorder.playing)
        {
            currentState = (int) State.FinishedAction;
            currentAmmonition.GetComponent<AirAmmo>().startFly();
            currentAmmonition = null;
        }
    }
    
    void waterShot()
    {
        if (!recorder.interpolating && recorder.lastPlayPosition > 5 && currentAmmonition == null)
        {
            currentAmmonition = Instantiate(preWater);
            aimTarget = Linker.manager.getRandomEnemyPosition(team);
            currentAmmonition.GetComponent<WaterAmmo>().setTarget(aimTarget);
        }
        if (currentAmmonition != null) currentAmmonition.GetComponent<WaterAmmo>().setLeftArm(character.getLeftHandPosition(), character.getLeftArmDirection());

        if (!recorder.interpolating && !turning && recorder.lastPlayPosition > 10 && recorder.lastPlayPosition < 13)
        {
            turnTo(aimTarget);
        }
        
        if (recorder.lastPlayPosition > 24)
        {
            currentAmmonition.GetComponent<WaterAmmo>().startFly();
        }

        
        if (!recorder.playing)
        {
            currentState = (int) State.FinishedAction;
            currentAmmonition.GetComponent<WaterAmmo>().startFly();
            currentAmmonition = null;
        }
    }
    
    void fireShot()
    {
        if (!recorder.interpolating && recorder.lastPlayPosition > 5 && currentAmmonition == null)
        {
            currentAmmonition = Instantiate(preFire);
            aimTarget = Linker.manager.getRandomEnemyPosition(team);
            currentAmmonition.GetComponent<FireAmmo>().setTarget(aimTarget);
        }
        if (currentAmmonition != null) currentAmmonition.GetComponent<FireAmmo>().setLeftArm(character.getLeftHandPosition(), character.getLeftArmDirection());

        if (!recorder.interpolating && !turning && recorder.lastPlayPosition > 10 && recorder.lastPlayPosition < 13)
        {
            turnTo(aimTarget);
        }
        
        if (recorder.lastPlayPosition > 20)
        {
            currentAmmonition.GetComponent<FireAmmo>().startFly();
        }

        
        if (!recorder.playing)
        {
            currentState = (int) State.FinishedAction;
            currentAmmonition.GetComponent<FireAmmo>().startFly();
            currentAmmonition = null;
        }
    }

    public Vector3 getAimPosition()
    {
        float height = character.getRootPosition().y;
        Vector3 stablePosition = Linker.manager.positions[team][side][position].transform.position;
        stablePosition.y = height;
        return stablePosition;
    }

}