using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ParserLogic
{
    enum Bone
    {
        Root,            // 0   Upper Chest 	 Root
        MidTorso,        // 1   Mid Torso        Child of Upper Chest
        LowerTorso,      // 2   Lower Torso      Child of Mid Torso
        LeftUpperLeg,    // 3   Left Upper Leg	 Child of Lower Torso
        LeftLowerLeg,    // 4   Left Lower Leg	 Child of Left Upper Leg
        LeftFoot,        // 5   Left Foot		 Child of Left Lower Leg
        RightUpperLeg,   // 6   Right Upper Leg	 Child of Lower Torso
        RightLowerLeg,   // 7   Right Lower Leg	 Child of Right Upper Leg
        RightFoot,       // 8   Right Foot 		 Child of Right Lower Leg
        Neck,            // 9   Neck			 Child of Upper Chest
        Head,            // 10  Head 			 Child of Neck
        RightClavicle,   // 11  Right Clavicle	 Child of Upper Chest
        RightShoulder,   // 12  Right Shoulder	 Child of Clavicle
        RightUpperArm,   // 13  Right Upper Arm	 Child of Shoulder
        RightLowerArm,   // 14  Right Lower Arm	 Child of Upper Arm
        RightHand,       // 15  Right Hand		 Child of Lower Arm
        LeftClavicle,    // 16  Left Clavicle	 Child of Upper Chest
        LeftShoulder,    // 17  Left Shoulder	 Child of Left Clavicle
        LeftUpperArm,    // 18  Left Upper Arm	 Child of Left Shoulder
        LeftLowerArm,    // 19  Left Lower Arm	 Child of Left Upper Arm
        LeftHand         // 20  Left Hand		 Child of Left Lower Arm
    }

    enum Precision
    {
        distanceHigh = 10,
        distanceLow = 20,
        distanceHigherThan = 40,
        middle = 50,
        high = 60,
        super = 70,
        ultra = 80,
        perfect = 90
    }

    public string[] positionFilenames = new string[25];
    public int[][][] instructions = new int[25][][];
    public int index = 0;
    
    public ParserLogic()
    {
        // ---------------------------------------- AIR
        addInstructionSet(
            "air_start", 
            new int [][,]
                {
                    new int[,] 
                        { 
                            { (int) Precision.ultra, (int) Bone.RightShoulder, (int) Bone.RightHand }, 
                            { (int) Precision.ultra, (int) Bone.LeftShoulder, (int) Bone.LeftHand }, 
                            { (int) Precision.ultra, (int) Bone.MidTorso, (int) Bone.RightHand },
                            { (int) Precision.ultra, (int) Bone.MidTorso, (int) Bone.LeftHand },
                            { (int) Precision.ultra, (int) Bone.RightHand, (int) Bone.LeftHand },
                        }
                }
        );
        
        addInstructionSet(
            "air_1", 
            new int [][,]
            {
                new int[,] 
                        { 
                            { (int) Precision.distanceLow, (int) Bone.RightShoulder, (int) Bone.RightHand },
                            { (int) Precision.distanceLow, (int) Bone.LeftShoulder, (int) Bone.LeftHand },
                        }
            }
        );
        
        addInstructionSet(
            "air_2", 
            new int [][,]
            {
                new int[,] 
                        { 
                            { (int) Precision.super, (int) Bone.LeftShoulder, (int) Bone.LeftHand }, 
                            { (int) Precision.distanceLow, (int) Bone.LeftShoulder, (int) Bone.LeftHand},
                            { (int) Precision.super, (int) Bone.RightShoulder, (int) Bone.RightHand }, 
                            { (int) Precision.distanceLow, (int) Bone.RightShoulder, (int) Bone.RightHand},
                        }
            }
        );
        
        // ---------------------------------------- EARTH
        
        addInstructionSet(
            "earth_start", 
            new int [][,]
            {
                new int[,] 
                        {
                            { (int) Precision.super, (int) Bone.RightShoulder, (int) Bone.RightHand }, 
                            { (int) Precision.super, (int) Bone.LeftShoulder, (int) Bone.LeftHand },
                            { (int) Precision.super, (int) Bone.LowerTorso, (int) Bone.RightUpperLeg },
                            { (int) Precision.super, (int) Bone.RightUpperLeg, (int) Bone.RightLowerLeg },
                            { (int) Precision.super, (int) Bone.RightLowerLeg, (int) Bone.RightFoot},
                            { (int) Precision.super, (int) Bone.LowerTorso, (int) Bone.LeftUpperLeg},
                            { (int) Precision.super, (int) Bone.LeftUpperLeg, (int) Bone.LeftLowerLeg},
                            { (int) Precision.super, (int) Bone.LeftUpperLeg, (int) Bone.LeftFoot},
                             { (int) Precision.super, (int) Bone.Head, (int) Bone.LeftFoot},
                             { (int) Precision.super, (int) Bone.Head, (int) Bone.RightFoot},
                            { (int) Precision.distanceHigh, (int) Bone.RightFoot, (int) Bone.LeftFoot}
                }
            }
        );
        
        addInstructionSet(
            "earth_1", 
            new int [][,]
            {
                new int[,] 
                        {
                            { (int) Precision.super, (int) Bone.RightShoulder, (int) Bone.RightUpperArm }, 
                            { (int) Precision.super, (int) Bone.RightUpperArm, (int) Bone.RightLowerArm },
                            { (int) Precision.super, (int) Bone.RightLowerArm, (int) Bone.RightHand },
                            { (int) Precision.super, (int) Bone.LeftShoulder, (int) Bone.LeftUpperArm }, 
                            { (int) Precision.super, (int) Bone.LeftUpperArm, (int) Bone.LeftLowerArm },
                            { (int) Precision.super, (int) Bone.LeftLowerArm, (int) Bone.LeftHand },
                            { (int) Precision.super, (int) Bone.MidTorso, (int) Bone.LowerTorso }
                        }
            }
        );
        
        addInstructionSet(
            "earth_2", 
            new int [][,]
            {
                new int[,] 
                        {
                            { (int) Precision.ultra, (int) Bone.LeftShoulder, (int) Bone.LeftHand }, 
                            { (int) Precision.distanceLow, (int) Bone.LeftShoulder, (int) Bone.LeftHand},
                            { (int) Precision.ultra, (int) Bone.RightShoulder, (int) Bone.RightHand }, 
                            { (int) Precision.distanceLow, (int) Bone.RightShoulder, (int) Bone.RightHand},
                        }
            }
        );
        
        // ---------------------------------------- FIRE
        
        addInstructionSet(
            "fire_start", 
            new int [][,]
            {
                new int[,] 
                        {
                            { (int) Precision.super, (int) Bone.RightShoulder, (int) Bone.RightHand }, 
                            { (int) Precision.super, (int) Bone.LeftShoulder, (int) Bone.LeftHand }, 
                            { (int) Precision.high, (int) Bone.LowerTorso, (int) Bone.RightUpperLeg },
                            { (int) Precision.high, (int) Bone.RightUpperLeg, (int) Bone.RightLowerLeg },
                            { (int) Precision.high, (int) Bone.RightLowerLeg, (int) Bone.RightFoot},
                            { (int) Precision.high, (int) Bone.LowerTorso, (int) Bone.LeftUpperLeg},
                            { (int) Precision.high, (int) Bone.LeftUpperLeg, (int) Bone.LeftLowerLeg},
                            { (int) Precision.high, (int) Bone.LeftUpperLeg, (int) Bone.LeftFoot},
                            { (int) Precision.distanceLow, (int) Bone.RightFoot, (int) Bone.LeftFoot}
                        }
            }
        );
        
        addInstructionSet(
            "fire_1", 
            new int [][,]
            {
                new int[,] 
                        {
                            { (int) Precision.ultra, (int) Bone.LeftShoulder, (int) Bone.LeftHand }, 
                            { (int) Precision.distanceLow, (int) Bone.LeftShoulder, (int) Bone.LeftHand},
                            { (int) Precision.distanceLow, (int) Bone.MidTorso, (int) Bone.LeftHand},
                            { (int) Precision.ultra, (int) Bone.MidTorso, (int) Bone.LowerTorso },
                        }
            }
        );
        
        addInstructionSet(
            "fire_2", 
            new int [][,]
            {
                new int[,] 
                        {
                            { (int) Precision.ultra, (int) Bone.LeftShoulder, (int) Bone.LeftHand },
                            { (int) Precision.distanceLow, (int) Bone.LeftShoulder, (int) Bone.LeftHand},
                        }
            }
        );
        
        // ---------------------------------------- WATER
        
        addInstructionSet(
            "water_start", 
            new int [][,]
            {
                new int[,] 
                        {
                            { (int) Precision.super, (int) Bone.RightShoulder, (int) Bone.RightUpperArm }, 
                            { (int) Precision.super, (int) Bone.RightUpperArm, (int) Bone.RightLowerArm },
                            { (int) Precision.super, (int) Bone.RightLowerArm, (int) Bone.RightHand },
                            { (int) Precision.super, (int) Bone.LeftShoulder, (int) Bone.LeftUpperArm }, 
                            { (int) Precision.super, (int) Bone.LeftUpperArm, (int) Bone.LeftLowerArm },
                            { (int) Precision.super, (int) Bone.LeftLowerArm, (int) Bone.LeftHand },
                            { (int) Precision.super, (int) Bone.LowerTorso, (int) Bone.RightUpperLeg },
                            { (int) Precision.super, (int) Bone.RightUpperLeg, (int) Bone.RightLowerLeg },
                            { (int) Precision.super, (int) Bone.RightLowerLeg, (int) Bone.RightFoot},
                            { (int) Precision.super, (int) Bone.LowerTorso, (int) Bone.LeftUpperLeg},
                            { (int) Precision.super, (int) Bone.LeftUpperLeg, (int) Bone.LeftLowerLeg},
                            { (int) Precision.super, (int) Bone.LeftUpperLeg, (int) Bone.LeftFoot},
                            { (int) Precision.distanceLow, (int) Bone.RightFoot, (int) Bone.LeftFoot}
                        }
            }
        );
        
        addInstructionSet(
            "water_1", 
            new int [][,]
            {
                new int[,] 
                        {
                            { (int) Precision.ultra, (int) Bone.LeftShoulder, (int) Bone.LeftUpperArm }, 
                            { (int) Precision.ultra, (int) Bone.LeftUpperArm, (int) Bone.LeftLowerArm },
                            { (int) Precision.ultra, (int) Bone.LeftLowerArm, (int) Bone.LeftHand },
                            { (int) Precision.distanceLow, (int) Bone.LeftShoulder, (int) Bone.LeftHand}
                        }
            }
        );
        
        addInstructionSet(
            "water_2", 
            new int [][,]
            {
                new int[,] 
                        {
                            
                            { (int) Precision.distanceHigh, (int) Bone.LeftShoulder, (int) Bone.LeftHand}
                        }
            }
        );
    }

    private void addInstructionSet(string name, int[][,] instructionSets)
    {
        positionFilenames[index] = name;
        int length = 0;
        foreach (int[,] set in instructionSets)
        {
            length += set.Length / 3;
        }
        
        instructions[index] = new int[length][];

        int instructionIndex = 0;
        foreach (int[,] set in instructionSets)
        {
            for (int i = 0; i < set.Length / 3; i++)
            {
                instructions[index][instructionIndex] = new int[3];
                instructions[index][instructionIndex][0] = set[i, 1];
                instructions[index][instructionIndex][1] = set[i, 2];
                instructions[index][instructionIndex][2] = set[i, 0];
                instructionIndex++;
            }
        }
        
        index++;
    }

    public int[][] getInstructionSet(string name)
    {
        int index = Array.IndexOf(positionFilenames, name);
        return instructions[index];
    }

    public int[,] preRightLeg()
    {
        return(
            new int[,] 
            { 
                { (int) Precision.high, (int) Bone.LowerTorso, (int) Bone.RightUpperLeg },
                { (int) Precision.high, (int) Bone.RightUpperLeg, (int) Bone.RightLowerLeg },
                { (int) Precision.high, (int) Bone.RightLowerLeg, (int) Bone.RightFoot},
            }
        );
    }
    
    public int[,] preLeftLeg()
    {
        return(
            new int[,] 
            { 
                { (int) Precision.high, (int) Bone.LowerTorso, (int) Bone.LeftUpperLeg},
                { (int) Precision.high, (int) Bone.LeftUpperLeg, (int) Bone.LeftLowerLeg},
                { (int) Precision.high, (int) Bone.LeftUpperLeg, (int) Bone.LeftFoot}
            }
        );
    }
    
    public int[,] preLegs()
    {
        return(
            new int[,] 
            { 
                { (int) Precision.high, (int) Bone.LowerTorso, (int) Bone.RightUpperLeg },
                { (int) Precision.high, (int) Bone.RightUpperLeg, (int) Bone.RightLowerLeg },
                { (int) Precision.high, (int) Bone.RightLowerLeg, (int) Bone.RightFoot},
                { (int) Precision.high, (int) Bone.LowerTorso, (int) Bone.LeftUpperLeg},
                { (int) Precision.high, (int) Bone.LeftUpperLeg, (int) Bone.LeftLowerLeg},
                { (int) Precision.high, (int) Bone.LeftUpperLeg, (int) Bone.LeftFoot}
            }
        );
    }
    
    public int[,] preTorso()
    {
        return(
            new int[,] 
            {
                { (int) Precision.high, (int) Bone.Root, (int) Bone.MidTorso },
                { (int) Precision.high, (int) Bone.MidTorso, (int) Bone.LowerTorso },
            }
        );
    }
    
    public int[,] preLeftArm()
    {
        return(
            new int[,] 
            { 
                { (int) Precision.high, (int) Bone.LeftShoulder, (int) Bone.LeftUpperArm }, 
                { (int) Precision.high, (int) Bone.LeftUpperArm, (int) Bone.LeftLowerArm },
                { (int) Precision.high, (int) Bone.LeftLowerArm, (int) Bone.LeftHand }
            }
        );
    }
    
    public int[,] preRightArm()
    {
        return(
            new int[,] 
            { 
                { (int) Precision.high, (int) Bone.RightShoulder, (int) Bone.RightUpperArm }, 
                { (int) Precision.high, (int) Bone.RightUpperArm, (int) Bone.RightLowerArm },
                { (int) Precision.high, (int) Bone.RightLowerArm, (int) Bone.RightHand }
            }
        );
    }
    
    // ATTENTION: root missing -> I think its better without it
    public int[,] preFullBody()
    {
        return(
            new int[,] 
            { 
                { (int) Precision.ultra, (int) Bone.RightShoulder, (int) Bone.RightUpperArm }, 
                { (int) Precision.perfect, (int) Bone.RightUpperArm, (int) Bone.RightLowerArm },
                { (int) Precision.perfect, (int) Bone.RightLowerArm, (int) Bone.RightHand },
                { (int) Precision.ultra, (int) Bone.LeftShoulder, (int) Bone.LeftUpperArm }, 
                { (int) Precision.perfect, (int) Bone.LeftUpperArm, (int) Bone.LeftLowerArm },
                { (int) Precision.perfect, (int) Bone.LeftLowerArm, (int) Bone.LeftHand },
                { (int) Precision.perfect, (int) Bone.MidTorso, (int) Bone.LowerTorso },
                { (int) Precision.ultra, (int) Bone.LowerTorso, (int) Bone.RightUpperLeg },
                { (int) Precision.perfect, (int) Bone.RightUpperLeg, (int) Bone.RightLowerLeg },
                { (int) Precision.perfect, (int) Bone.RightLowerLeg, (int) Bone.RightFoot},
                { (int) Precision.ultra, (int) Bone.LowerTorso, (int) Bone.LeftUpperLeg},
                { (int) Precision.perfect, (int) Bone.LeftUpperLeg, (int) Bone.LeftLowerLeg},
                { (int) Precision.perfect, (int) Bone.LeftUpperLeg, (int) Bone.LeftFoot}
            }
        );
    }

}