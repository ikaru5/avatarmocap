using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

// loads Recorded positions into memory and compares by call
public class PositionParser
{
    enum Bone
    {
        Root,            // 0   Upper Chest 	 Root
        MidTorso,        // 1   Mid Torso        Child of Upper Chest
        LowerTorso,      // 2   Lower Torso      Child of Mid Torso
        LeftUpperLeg,    // 3   Left Upper Leg	 Child of Lower Torso
        LeftlowerLeg,    // 4   Left Lower Leg	 Child of Left Upper Leg
        LeftFoot,        // 5   Left Foot		 Child of Left Lower Leg
        RightUpperLeg,   // 6   Right Upper Leg	 Child of Lower Torso
        RightLowerLeg,   // 7   Right Lower Leg	 Child of Right Upper Leg
        RightFoot,       // 8   Right Foot 		 Child of Right Lower Leg
        Neck,            // 9   Neck			 Child of Upper Chest
        Head,            // 10  Head 			 Child of Neck
        RightClavicle,   // 11  Right Clavicle	 Child of Upper Chest
        RightShoulder,   // 12  Right Shoulder	 Child of Clavicle
        RightUpperArm,   // 13  Right Upper Arm	 Child of Shoulder
        RightLowerArm,   // 14  Right Lower Arm	 Child of Upper Arm
        RightHand,       // 15  Right Hand		 Child of Lower Arm
        LeftClavicle,    // 16  Left Clavicle	 Child of Upper Chest
        LeftShoulder,    // 17  Left Shoulder	 Child of Left Clavicle
        LeftUpperArm,    // 18  Left Upper Arm	 Child of Left Shoulder
        LeftLowerArm,    // 19  Left Lower Arm	 Child of Left Upper Arm
        LeftHand         // 20  Left Hand		 Child of Left Lower Arm
    }
    
    public ComparablePosition[] positions;
    public string[] names;
    public ParserLogic parserLogic;
    
    public PositionParser(string[] positionNames, ref ParserLogic parserLogic)
    {
        names = positionNames;
        this.parserLogic = parserLogic;
        
        positions = new ComparablePosition[parserLogic.index];
        for (int i = 0; i < parserLogic.index; i++)
        {
            positions[i] = new ComparablePosition(positionNames[i]);
        }
    }

    public int getIndexOfPosition(string name)
    {
        return Array.IndexOf(names, name);
    }

    public bool compare(string positionName, ref ComparablePosition currentPosition)
    {
        int positionIndex = getIndexOfPosition(positionName);
        var returnVal = 
            positions[positionIndex].batchCompare(parserLogic.getInstructionSet(positionName), ref currentPosition);
        //if (returnVal) Debug.Log(returnVal);
        return returnVal;
    }
    
    
    
}