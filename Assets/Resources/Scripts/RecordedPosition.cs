using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class RecordedPosition
{
    public Vector3[] recordedPositions;
    public Quaternion[] recordedRotations;
    
    public RecordedPosition()
    {
        recordedPositions = new Vector3[21]; 
        recordedRotations = new Quaternion[21]; 
    }
    
    [Serializable]
    private struct SerializedRecord
    {
        public float[] px;
        public float[] py;
        public float[] pz;
        public float[] rx;
        public float[] ry;
        public float[] rz;
        public float[] rw;

        public SerializedRecord(Vector3[] position, Quaternion[] rotation)
        {
            px = new float[21];
            py = new float[21];
            pz = new float[21];
            
            rx = new float[21];
            ry = new float[21];
            rz = new float[21];
            rw = new float[21];
            
            for (int i = 0; i < 21; i++)
            {
                px[i] = position[i].x;
                py[i] = position[i].y;
                pz[i] = position[i].z;
            
                rx[i] = rotation[i].x;
                ry[i] = rotation[i].y;
                rz[i] = rotation[i].z;
                rw[i] = rotation[i].w;
            }
        }

        public RecordedPosition getPosition()
        {
            RecordedPosition position = new RecordedPosition();
            
            for (int i = 0; i < 21; i++)
            {
                position.recordedPositions[i].x = px[i];
                position.recordedPositions[i].y = py[i];
                position.recordedPositions[i].z = pz[i];
                
                position.recordedRotations[i].x = rx[i];
                position.recordedRotations[i].y = ry[i];
                position.recordedRotations[i].z = rz[i];
                position.recordedRotations[i].w = rw[i];
            }

            return position;
        }
    }
    
    // serialized is not flexible enough during development. use json and save as serialized for production. :)
    public void saveSerialized(string filename) {
        string filePath = Application.dataPath + "/recordings/" + filename + "_position.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        SerializedRecord data = new SerializedRecord(recordedPositions, recordedRotations);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Record saved:" + filePath);
    }
    
    public void save(string filename) {
        string filePath = Application.dataPath + "/recordings/" + filename + "_position.json";
        SerializedRecord data = new SerializedRecord(recordedPositions, recordedRotations);
        File.WriteAllText(filePath, JsonUtility.ToJson(data));
        Debug.Log("Record saved:" + filePath);
    }

    public static RecordedPosition load(string filename)
    {
        string filePath = Application.dataPath + "/recordings/" + filename + "_position.json";
        if (File.Exists(filePath)) {
            var json = File.ReadAllText(filePath);
            SerializedRecord data = JsonUtility.FromJson<SerializedRecord>(json);
            RecordedPosition position = data.getPosition();
            return position;
        }
        Debug.Log("Could not load record: " + filePath);
        return null;
    }
    
    public static RecordedPosition loadSerialized(string filename) {
        string filePath = Application.dataPath + "/recordings/" + filename + "_position.dat";
        if (File.Exists(filePath)) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(filePath, FileMode.Open);
            SerializedRecord data = (SerializedRecord)bf.Deserialize(file);
            RecordedPosition position = data.getPosition();
            file.Close();

            return position;
        }
        Debug.Log("Could not load record: " + filePath);
        return null;
    }
}