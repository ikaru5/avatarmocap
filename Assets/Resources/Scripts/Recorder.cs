using System;
using UnityEngine;

public class Recorder
{
    private Transform[] bones;
    // prepare some space in RAM for multiple positions and records
    private Recording[] records;
    private RecordedPosition[] positions;

    // recording stuff
    public int lastPlayPosition = 0; // array index within recording
    private int currentRecord = 0;
    
    // playback recording stuff
    private int currentPlayedRecord = 0;
    private float playBackSpeed = 1;
    private float currentPlayTime = 0;


    // recorded position stuff
    private int currentPlayedRecordedPosition = 0;

    // accessors for current frame
    public Vector3[] currentPlayPositions;
    public Quaternion[] currentPlayRotations;

    // for interpolation
    private Vector3[] originPlayPositions;
    private Vector3[] targetPlayPositions;
    private float interpolateProgress = 0;
    
    // state
    public bool recording;
    public bool playing;
    public bool pausing;
    public bool showPosition;
    public bool loop;
    public bool interpolate;
    public bool interpolating;
    private bool playOnly = false;

    private RecorderInterface recorderInterface;
    
    public Recorder(Transform[] bones, Boolean onlyPlayback = false, int recordCount = 5, int positionCount = 20)
    {
        records = new Recording[recordCount];
        for (int i = 0; i < recordCount; i++)
        {
            records[i] = new Recording();
        }
        
        positions = new RecordedPosition[positionCount];
        for (int i = 0; i < positionCount; i++)
        {
            positions[i] = new RecordedPosition();
        }
        
        currentPlayPositions = new Vector3[21];
        currentPlayRotations = new Quaternion[21];
        
        originPlayPositions = new Vector3[21];
        targetPlayPositions = new Vector3[21];

        this.bones = bones;
        
        recording = false;
        playing = false;
        pausing = false;
        showPosition = false;
        
        recorderInterface = GameObject.Find("Avatar").GetComponent<RecorderInterface>();
    }

    //---------------------------------- Methods
    public void update()
    {
        // for the small interface
        if (!playOnly)
        {
            recorderInterface.parseInput(this);
        }
        
        run();
    }

    private void run()
    {
        if (recording && !records[currentRecord].add(bones))
        {
            Debug.Log("Recording Limit reached!");
            recording = false;
        }
    }
    
    public void loadNextFrame()
    {
        if (showPosition)
        {
            for (int i = 0; i < 21; i++)
            {
                currentPlayPositions[i] = positions[currentPlayedRecordedPosition].recordedPositions[i];
            
                currentPlayRotations[i] = positions[currentPlayedRecordedPosition].recordedRotations[i];
            }

            return;
        }

        if (interpolating)
        {
            interpolateProgress += Time.deltaTime * 2;
            if (interpolateProgress >= 1)
            {
                interpolating = false;
                for (int i = 0; i < 21; i++)
                {
                    currentPlayPositions[i] = targetPlayPositions[i];
                }
            }
            else
            {
                for (int i = 0; i < 21; i++)
                {
                    currentPlayPositions[i] = Vector3.Lerp(originPlayPositions[i], targetPlayPositions[i], interpolateProgress);
                }
            }
            
            return;
        }
        
        if (pausing)
        {
            currentPlayTime = records[currentPlayedRecord].timeRecorded[lastPlayPosition];
        }
        else
        {
            while (currentPlayTime > records[currentPlayedRecord].timeRecorded[lastPlayPosition])
            {
                lastPlayPosition++;
                if (lastPlayPosition >= records[currentPlayedRecord].end)
                {
                    if (interpolate)
                    {
                        if (loop)
                        {
                            //Debug.Log("Looping");
                            interpolating = true;
                            for (int i = 0; i < 21; i++)
                            {
                                originPlayPositions[i] = currentPlayPositions[i];
                                targetPlayPositions[i] = records[currentPlayedRecord].recordedPositions[i] + records[currentPlayedRecord].displacement;
                            }

                            interpolateProgress = 0;
                            currentPlayTime = 0;
                            lastPlayPosition = 0;
                        }
                        else
                        {
                            //Debug.Log("saving for interpolate");
                            interpolating = true;
                            for (int i = 0; i < 21; i++)
                            {
                                originPlayPositions[i] = currentPlayPositions[i];
                            }

                            interpolateProgress = 0;
                            playing = false;
                        }
                    }
                    else
                    {
                        playing = false;
                    }

                    return;
                }
            } 
        }
        
        // for the first frame
        if (lastPlayPosition == 0)
        {
            for (int i = 0; i < 21; i++)
            {
                currentPlayPositions[i] = records[currentPlayedRecord].recordedPositions[lastPlayPosition + i] + records[currentPlayedRecord].displacement;
            
                currentPlayRotations[i] = records[currentPlayedRecord].recordedRotations[lastPlayPosition + i];
            }
            currentPlayTime += Time.deltaTime;
            return;
        }
        
//        Debug.Log("Playing Position " + lastPlayPosition);
        float deltaBetweenFrames = records[currentPlayedRecord].timeRecorded[lastPlayPosition] -
                  records[currentPlayedRecord].timeRecorded[lastPlayPosition - 1];
        float timeBetweenFrames = currentPlayTime - records[currentPlayedRecord].timeRecorded[lastPlayPosition - 1];
        
        if (timeBetweenFrames > deltaBetweenFrames) Debug.Log("Something went wrong!!! Need Debug");
        
        int previousFramePosition = (lastPlayPosition - 1) * 21;
        int currentFramePosition = lastPlayPosition * 21;
        
//        Debug.Log("Playing Times " + deltaBetweenFrames + " - " + timeBetweenFrames + " - " + timeBetweenFrames / deltaBetweenFrames);
        for (int i = 0; i < 21; i++)
        {
            currentPlayPositions[i] = 
                Vector3.Lerp(
                    records[currentPlayedRecord].recordedPositions[previousFramePosition + i] + records[currentPlayedRecord].displacement,
                    records[currentPlayedRecord].recordedPositions[currentFramePosition + i] + records[currentPlayedRecord].displacement,
                    timeBetweenFrames / deltaBetweenFrames
                );
            
            currentPlayRotations[i] = 
                Quaternion.Lerp(
                    records[currentPlayedRecord].recordedRotations[previousFramePosition + i],
                    records[currentPlayedRecord].recordedRotations[currentFramePosition + i],
                    timeBetweenFrames / deltaBetweenFrames
                );
        }
        
        currentPlayTime += (playBackSpeed * Time.deltaTime);
    }
    
    //---------------------------------------- control methods
    
    public void play()
    {
        playing = true;
        pausing = false;
    }

    public void rec(bool value)
    {
        recording = value; 
    }

    public void stop()
    {
        playing = false;
        pausing = false;
        rec(false);
    }
    
    public void pause()
    {
        playing = true;
        pausing = true;
    }

    public int stepBack()
    {
        lastPlayPosition--;
        return lastPlayPosition;
    }
    
    public int stepForward()
    {
        lastPlayPosition++;
        return lastPlayPosition;
    }
    
    public void rewind(float time = 0)
    {
        currentPlayTime = time;
        lastPlayPosition = 0;
    }
    
    public void setPlayBackSpeed(float number)
    {
        playBackSpeed = number;
    }

    public int getCurrentRecord()
    {
        return(currentRecord);
    }
    
    public void setCurrentRecord(int number)
    {
        currentRecord = number;
    }
    
    public int getCurrentPlayRecord()
    {
        return(currentPlayedRecord);
    }
    
    public void setCurrentPlayRecord(int number)
    {
        if (interpolate)
        {
            for (int i = 0; i < 21; i++)
            {
                targetPlayPositions[i] = records[number].recordedPositions[i] + records[number].displacement;
                originPlayPositions[i] = currentPlayPositions[i];
            }

            interpolateProgress = 0;
            interpolating = true;
        }
        currentPlayedRecord = number;
    }
    
    public int getCurrentPlayedRecordedPosition()
    {
        return(currentPlayedRecordedPosition);
    }
    
    public void setCurrentPlayedRecordedPosition(int number)
    {
        currentPlayedRecordedPosition = number;
    }
    
    public void load(string filename, int number, Vector3 displacement = new Vector3())
    {
        currentRecord = number;
        records[currentRecord] = Recording.load(filename);
        records[currentRecord].displacement = displacement;
    }
    
    public void save(string filename)
    {
        records[currentRecord].save(filename);
    }
    
    public void loadSerialized(string filename, int number)
    {
        currentRecord = number;
        records[currentRecord] = Recording.loadSerialized(filename);
    }
    
    public void saveSerialized(string filename)
    {
        records[currentRecord].saveSerialized(filename);
    }
    
    // Mocap recorded everthing in about 100fps, better cut the most away and interpolate between rest of it
    public void sanitize(int number)
    {
        records[currentRecord] = records[currentRecord].getSanitized(number);
    }
    
    public void cut(int from, int to)
    {
        records[currentRecord] = records[currentRecord].getCutted(from, to);
    }

    public void savePosition(int positionFrameNumber, string positionFilename)
    {
        records[currentRecord].savePosition(positionFrameNumber, positionFilename);
    }
    
    public void loadPosition(string positionFilename)
    {
        positions[currentPlayedRecordedPosition] = RecordedPosition.load(positionFilename);
    }

    public bool toggleShowPosition()
    {
        showPosition = !showPosition;
        return showPosition;
    }
}