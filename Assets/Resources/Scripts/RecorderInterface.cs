﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecorderInterface : MonoBehaviour
{

    public string status;
    public int currentRecord;
    public int currentPlayRecord;
    public Boolean getCurrentRecord;
    public Boolean setCurrentRecord;
    public Boolean getCurrentPlayRecord;
    public Boolean setCurrentPlayRecord;
    public Boolean play;
    public Boolean pause;
    public Boolean stepBack;
    public Boolean stepForward;
    public Boolean stop;
    public Boolean rewind;
    public Boolean record;
    public string filename;
    public Boolean load;
    public Boolean save;
    public Boolean loadSerialized;
    public Boolean saveSerialized;
    public Boolean setPlayBackSpeed;
    public float playBackSpeed = 1;

    public int keepEverThFrame;
    public Boolean sanitize;

    public int cutFrom;
    public int cutTo;
    public Boolean cut;

    // position stuff
    public bool toggleShowPosition;
    public string positionFilename;

    // save
    public int positionFrameNumber;
    public Boolean savePosition;

    // load
    public int currentPlayedRecordedPosition;
    public Boolean getCurrentPlayedRecordedPosition;
    public Boolean setCurrentPlayedRecordedPosition;
    public bool loadPosition = false;

    public void parseInput(Recorder recorder)
    {
        if (record)
        {
            status = "Start Recording";
            recorder.rec(true);
            record = false;
        }
            
        if (stop)
        {
            status = "Stop";
            recorder.stop();
            stop = false;
        }
        
        if (pause)
        {
            status = "Pause";
            recorder.pause();
            pause = false;
        }
        
        if (stepBack)
        {
            status = "step back to " + recorder.stepBack();
            stepBack = false;
        }
        
        if (stepForward)
        {
            status = "step forward to " + recorder.stepForward();
            stepForward = false;
        }
        
        if (rewind)
        {
            status = "Rewind top position 0 recording " + currentRecord;
            recorder.rewind();
            rewind = false;
        }
            
        if (play)
        {
            status = "Play Recording";
            recorder.play();
            play = false;
        }
                
        if (getCurrentRecord)
        {
            currentRecord = recorder.getCurrentRecord();
            getCurrentRecord = false;
        }
                
        if (setCurrentRecord)
        {
            recorder.setCurrentRecord(currentRecord);
            setCurrentRecord = false;
        }
        
        if (getCurrentPlayRecord)
        {
            currentPlayRecord = recorder.getCurrentPlayRecord();
            getCurrentPlayRecord = false;
        }
                
        if (setCurrentPlayRecord)
        {
            recorder.setCurrentPlayRecord(currentPlayRecord);
            setCurrentPlayRecord = false;
        }

        if (load)
        {
            status = "Loading file to " + currentRecord;
            recorder.load(filename, currentRecord);
            load = false;
        }
        
        if (save)
        {
            status = "Saving file as " + filename;
            recorder.save(filename);
            save = false;
        }
        
        if (loadSerialized)
        {
            status = "Loading file to " + currentRecord;
            recorder.loadSerialized(filename, currentRecord);
            loadSerialized = false;
        }
        
        if (saveSerialized)
        {
            status = "Saving file as " + filename;
            recorder.saveSerialized(filename);
            saveSerialized = false;
        }
        
        if (sanitize)
        {
            status = "Cutting redundant frames";
            recorder.sanitize(keepEverThFrame);
            sanitize = false;
        }

        if (setPlayBackSpeed)
        {
            status = "Setting PlayBack Speed to " + playBackSpeed;
            recorder.setPlayBackSpeed(playBackSpeed);
            setPlayBackSpeed = false;
        }
        
        // WARNING! Not cutting doesnt mean cutting out! You will get the thing between from and to.
        if (cut)
        {
            status = "Cutting recording from " + cutFrom + " and to " + cutTo;
            recorder.cut(cutFrom, cutTo);
            cut = false;
        }
        
        if (savePosition)
        {
            status = "Saving Position of frame " + positionFrameNumber + " to " + positionFilename + "_position.json";
            recorder.savePosition(positionFrameNumber, positionFilename);
            savePosition = false;
        }

        if (getCurrentPlayedRecordedPosition)
        {
            currentPlayedRecordedPosition = recorder.getCurrentPlayedRecordedPosition();
            getCurrentPlayedRecordedPosition = false;
        }

        if (setCurrentPlayedRecordedPosition)
        {
            status = "Setting currentPlayedRecordedPostion to " + currentPlayedRecordedPosition;
            recorder.setCurrentPlayedRecordedPosition(currentPlayedRecordedPosition);
            setCurrentPlayedRecordedPosition = false;
        }
        
        if (loadPosition)
        {
            status = "Loading position " + positionFilename;
            recorder.loadPosition(positionFilename);
            loadPosition = false;
        }

        if (toggleShowPosition)
        {
            status = "showing position: " + recorder.toggleShowPosition();
            toggleShowPosition = false;
        }
    }
}
