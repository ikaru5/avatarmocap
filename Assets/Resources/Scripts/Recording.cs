using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Recording
{
    // needed for displaced records
    public Vector3 displacement = new Vector3(0,0,0);
    
    public Vector3[] recordedPositions;
    public Quaternion[] recordedRotations;
    public float[] timeRecorded;
    public int end = 0;
    public static int maxDuration = 90 * 60 * 30; // fps * seconds * MIN
    
    public Recording(int duration = -1)
    {
        if (-1 == duration) duration = maxDuration;
        recordedPositions = new Vector3[duration * 21]; 
        recordedRotations = new Quaternion[duration * 21]; 
        timeRecorded = new float[duration];
    }

    public bool add(Transform[] bones)
    {
        if (end < maxDuration)
        {
            // record time
            if (end == 0)
            {
                timeRecorded[0] = 0;
            }
            else
            {
                timeRecorded[end] = timeRecorded[end - 1] + Time.deltaTime;
            }
            
            // record bones
            for (int i = 0; i < 21; i++)
            {
                recordedPositions[i + 21 * end] = bones[i].localPosition;
                recordedRotations[i + 21 * end] = bones[i].localRotation;
            }

            end++;
            return true;
        }

        return false;
    }
    
    public Recording getSanitized(int keepEveryThFrame)
    {
        int size = (end / keepEveryThFrame) + 1;
        int counter = 0;    
        Recording recording = new Recording(size);
        
        
        for (int newEnd = 0; newEnd < end; newEnd += keepEveryThFrame)
        {
            if (newEnd > end) break;
            recording.end = counter + 1;
            
            recording.timeRecorded[counter] = timeRecorded[newEnd];
            
            // copy bones
            for (int i = 0; i < 21; i++)
            {
                recording.recordedPositions[i + 21 * counter] = recordedPositions[i + 21 * newEnd];
                recording.recordedRotations[i + 21 * counter] = recordedRotations[i + 21 * newEnd];
            }
            
            counter++;
        }

        return recording;
    }
    
    public Recording getCutted(int from, int to)
    {
        int size = to - from;
        Recording recording = new Recording(size);
        
        
        for (int counter = 0; counter < size; counter++)
        {
            recording.end = size;
            
            recording.timeRecorded[counter] = timeRecorded[counter + from];
            
            // copy bones
            for (int i = 0; i < 21; i++)
            {
                recording.recordedPositions[i + 21 * counter] = recordedPositions[i + 21 * (counter + from)];
                recording.recordedRotations[i + 21 * counter] = recordedRotations[i + 21 * (counter + from)];
            }
        }

        return recording;
    }

    [Serializable]
    private struct SerializedRecord
    {
        public float[] px;
        public float[] py;
        public float[] pz;
        public float[] rx;
        public float[] ry;
        public float[] rz;
        public float[] rw;
        public float[] timeRecorded;
        public int end;

        public SerializedRecord(int end, Vector3[] position, Quaternion[] rotation, float[] timeRecorded)
        {
            int size = end;
            px = new float[size * 21];
            py = new float[size * 21];
            pz = new float[size * 21];
            
            rx = new float[size * 21];
            ry = new float[size * 21];
            rz = new float[size * 21];
            rw = new float[size * 21];
            this.timeRecorded = new float[size];
            
            for (int i = 0; i < end; i++)
            {
                this.timeRecorded[i] = timeRecorded[i];
            }
            
            for (int i = 0; i < end * 21; i++)
            {
                px[i] = position[i].x;
                py[i] = position[i].y;
                pz[i] = position[i].z;
            
                rx[i] = rotation[i].x;
                ry[i] = rotation[i].y;
                rz[i] = rotation[i].z;
                rw[i] = rotation[i].w;
            }

            this.end = end;
        }

        public Recording getRecording()
        {
            // on recording the end var is increasing 1 too much, heavy to fix... maybe later
            if (timeRecorded.Length <= end || timeRecorded[end] == 0)
            {
                end--;
            }
            
            int size = end * 21;
            
            Recording recording = new Recording(end);
            
            for (int t = 0; t < end; t++)
            {
                recording.timeRecorded[t] = timeRecorded[t];
            }
            
            recording.end = end;

            for (int i = 0; i < size; i++)
            {
                recording.recordedPositions[i].x = px[i];
                recording.recordedPositions[i].y = py[i];
                recording.recordedPositions[i].z = pz[i];
                
                recording.recordedRotations[i].x = rx[i];
                recording.recordedRotations[i].y = ry[i];
                recording.recordedRotations[i].z = rz[i];
                recording.recordedRotations[i].w = rw[i];
            }

            return recording;
        }
    }
    
    public void savePosition(int positionFrameNumber, string positionFilename)
    {
        Recording cuttedRecording = getCutted(positionFrameNumber, positionFrameNumber + 1);
        RecordedPosition position = new RecordedPosition();
        for (int i = 0; i < 21; i++)
        {
            position.recordedPositions[i] = cuttedRecording.recordedPositions[i];
            position.recordedRotations[i] = cuttedRecording.recordedRotations[i];
        }
        position.save(positionFilename);
    }

    // serialized is not flexible enough during development. use json and save as serialized for production. :)
    public void saveSerialized(string filename) {
        string filePath = Application.dataPath + "/recordings/" + filename + "_recording.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        SerializedRecord data = new SerializedRecord(end, recordedPositions, recordedRotations, timeRecorded);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Record saved: " + filePath);
    }
    
    public void save(string filename) {
        string filePath = Application.dataPath + "/recordings/" + filename + "_recording.json";
        SerializedRecord data = new SerializedRecord(end, recordedPositions, recordedRotations, timeRecorded);
        File.WriteAllText(filePath, JsonUtility.ToJson(data));
        Debug.Log("Record saved: " + filePath);
    }

    public static Recording load(string filename) {
        string filePath = Application.dataPath + "/recordings/" + filename + "_recording.json";
        if (File.Exists(filePath)) {
            var json = File.ReadAllText(Application.dataPath + "/recordings/" + filename + "_recording.json");
            SerializedRecord data = JsonUtility.FromJson<SerializedRecord>(json);
            Recording recording = data.getRecording();
            return recording;
        }
        Debug.Log("Could not load record: " + filePath);
        return null;
    }
    
    public static Recording loadSerialized(string filename) {
        string filePath = Application.dataPath + "/recordings/" + filename + "_recording.dat";
        if (File.Exists(filePath)) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(filePath, FileMode.Open);
            SerializedRecord data = (SerializedRecord)bf.Deserialize(file);
            Recording recording = data.getRecording();
            file.Close();

            return recording;
        }
        Debug.Log("Could not load record: " + filePath);
        return null;
    }
}