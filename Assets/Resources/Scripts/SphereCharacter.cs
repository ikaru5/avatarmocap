using UnityEngine;

public class SphereCharacter : Character
{
    enum Bone
    {
        Root,            // 0   Upper Chest 	 Root
        MidTorso,        // 1   Mid Torso        Child of Upper Chest
        LowerTorso,      // 2   Lower Torso      Child of Mid Torso
        LeftUpperLeg,    // 3   Left Upper Leg	 Child of Lower Torso
        LeftLowerLeg,    // 4   Left Lower Leg	 Child of Left Upper Leg
        LeftFoot,        // 5   Left Foot		 Child of Left Lower Leg
        RightUpperLeg,   // 6   Right Upper Leg	 Child of Lower Torso
        RightLowerLeg,   // 7   Right Lower Leg	 Child of Right Upper Leg
        RightFoot,       // 8   Right Foot 		 Child of Right Lower Leg
        Neck,            // 9   Neck			 Child of Upper Chest
        Head,            // 10  Head 			 Child of Neck
        RightClavicle,   // 11  Right Clavicle	 Child of Upper Chest
        RightShoulder,   // 12  Right Shoulder	 Child of Clavicle
        RightUpperArm,   // 13  Right Upper Arm	 Child of Shoulder
        RightLowerArm,   // 14  Right Lower Arm	 Child of Upper Arm
        RightHand,       // 15  Right Hand		 Child of Lower Arm
        LeftClavicle,    // 16  Left Clavicle	 Child of Upper Chest
        LeftShoulder,    // 17  Left Shoulder	 Child of Left Clavicle
        LeftUpperArm,    // 18  Left Upper Arm	 Child of Left Shoulder
        LeftLowerArm,    // 19  Left Lower Arm	 Child of Left Upper Arm
        LeftHand         // 20  Left Hand		 Child of Left Lower Arm
    }
    
    private GameObject[] sphere; //Sphären um Knochenpositionen anzuzeigen

    private Transform parentTransform;

    public SphereCharacter(GameObject parent)
    {
        sphere = new GameObject[21];
        for (int i = 0; i < 21; i++)
        {
            if (i == 5 || i == 8 || i == 10 || i == 15 || i == 20)
                sphere[i] = (GameObject) GameObject.Instantiate(Resources.Load("Prefabs/SphereCollider"),
                    new Vector3(0f, 0f, 0f), Quaternion.identity); //Sphären Instanzieren und Erstellen
            else
                sphere[i] = (GameObject) GameObject.Instantiate(Resources.Load("Prefabs/Sphere"),
                    new Vector3(0f, 0f, 0f), Quaternion.identity); //Sphären Instanzieren und Erstellen
            sphere[i].transform.parent = parent.transform;
        }
    }

    public override void setCollidingLimbsActive(bool state)
    {
        sphere[5].GetComponent<Collider>().enabled = state;
        sphere[8].GetComponent<Collider>().enabled = state;
        sphere[10].GetComponent<Collider>().enabled = state;
        sphere[15].GetComponent<Collider>().enabled = state;
        sphere[20].GetComponent<Collider>().enabled = state;
    }
    
    public override void updateBone(int index, Vector3 position, Quaternion rotation)
    {
        sphere[index].transform.localPosition = position;
    }
    
    public override Vector3 getRootPosition()
    {
        return sphere[(int) Bone.Root].transform.position;
    }
    
    public override Vector3 getLocalRootPosition()
    {
        return sphere[(int) Bone.Root].transform.localPosition;
    }
    
    public override Vector3 getLeftArmDirection()
    {
        return sphere[(int) Bone.LeftHand].transform.position - sphere[(int) Bone.LeftShoulder].transform.position;
    }
    
    public override Vector3 getRightArmDirection()
    {
        return sphere[(int) Bone.RightHand].transform.position - sphere[(int) Bone.RightShoulder].transform.position;
    }

    public override Vector3 getLeftHandPosition()
    {
        return sphere[(int) Bone.LeftHand].transform.position;
    }
    
    public override Vector3 getRightHandPosition()
    {
        return sphere[(int) Bone.RightHand].transform.position;
    }
}