﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartButton : MonoBehaviour
{

    public bool debugStart = false;

    private void Update()
    {
        if (debugStart)
        {
            Linker.manager.newGame();
            debugStart = false;
        }
    }

    public void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Player")
        {
            Linker.manager.newGame();
        }
    }
}
